-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2015 at 10:03 
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `USER` varchar(20) NOT NULL,
  `PASS` text,
  PRIMARY KEY (`USER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`USER`, `PASS`) VALUES
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Table structure for table `detail_bimbingan`
--

DROP TABLE IF EXISTS `detail_bimbingan`;
CREATE TABLE IF NOT EXISTS `detail_bimbingan` (
  `ID_DETAIL_BIMBINGAN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_KP` int(11) DEFAULT NULL,
  `TOPIK_BIMBINGAN` text,
  `TANGGAL_BIMBINGAN` date DEFAULT NULL,
  `STATUS_DETAIL_BIMBINGAN` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID_DETAIL_BIMBINGAN`),
  KEY `FK_RELATIONSHIP_5` (`ID_KP`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `detail_bimbingan`
--

INSERT INTO `detail_bimbingan` (`ID_DETAIL_BIMBINGAN`, `ID_KP`, `TOPIK_BIMBINGAN`, `TANGGAL_BIMBINGAN`, `STATUS_DETAIL_BIMBINGAN`) VALUES
(12, 8, 'Topik Bimbingan 2 test', '2015-05-30', NULL),
(13, 11, 'Topik Bahasan 1', '2015-05-02', NULL),
(11, 8, 'Struktur Basisdata Web23', '2015-06-17', NULL),
(14, 11, 'Struktur Basisdata Web', '2015-05-14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
CREATE TABLE IF NOT EXISTS `dosen` (
  `NIP` varchar(20) NOT NULL,
  `NAMA_DOSEN` varchar(50) DEFAULT NULL,
  `PASSWORD_DOSEN` text,
  `TELP_DOSEN` varchar(12) DEFAULT NULL,
  `ALAMAT_DOSEN` text,
  `STATUS_DOSEN` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`NIP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`NIP`, `NAMA_DOSEN`, `PASSWORD_DOSEN`, `TELP_DOSEN`, `ALAMAT_DOSEN`, `STATUS_DOSEN`) VALUES
('511111111101', 'Ferry F. F', 'bb752e84c61e1eb27fc1b1f2f232f70ba7947189', '085566778899', 'Jl. Perumdos Sala', 'Dosbing'),
('511111111102', 'Donny D. D', 'ce3eaa938d09504bae9458dffb805f2de7c9da4e', '031-5997755', 'Jl. Perumdos Jambu Biji 1, Surabaya', 'Dosbing'),
('22222222222222222222', 'Dosen 2', 'ce3eaa938d09504bae9458dffb805f2de7c9da4e', '0833333333', 'Jl. Perumdos Salak 12C, Surabaya', 'Koor'),
('11111111111111111111', 'Dosen 1', 'ce3eaa938d09504bae9458dffb805f2de7c9da4e', '031-5975555', 'Jl. Perumdos Jambu Biji 1, Surabaya', 'Dosbing'),
('511111111103', 'Elly E. E', 'ce3eaa938d09504bae9458dffb805f2de7c9da4e', '031-5975555', 'Jl. Perumdos Semangka 3 / 40C, Surabaya', 'Dosbing');

-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

DROP TABLE IF EXISTS `kelompok`;
CREATE TABLE IF NOT EXISTS `kelompok` (
  `ID_KELOMPOK` int(11) NOT NULL AUTO_INCREMENT,
  `NRP` varchar(11) DEFAULT NULL,
  `MHS_NRP` varchar(11) DEFAULT NULL,
  `STATUS_KELOMPOK` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID_KELOMPOK`),
  KEY `FK_MHS1` (`MHS_NRP`),
  KEY `FK_MHS2` (`NRP`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `kelompok`
--

INSERT INTO `kelompok` (`ID_KELOMPOK`, `NRP`, `MHS_NRP`, `STATUS_KELOMPOK`) VALUES
(45, '5111100074', '5111100112', 6),
(49, '5111100004', '5111100007', 6),
(48, '5111100002', '5111100003', 6),
(47, '5111100008', '5111100008', 5),
(46, '5111100009', '5111100001', 4);

-- --------------------------------------------------------

--
-- Table structure for table `kp`
--

DROP TABLE IF EXISTS `kp`;
CREATE TABLE IF NOT EXISTS `kp` (
  `ID_KP` int(11) NOT NULL AUTO_INCREMENT,
  `NIP` varchar(20) DEFAULT NULL,
  `ID_KELOMPOK` int(11) DEFAULT NULL,
  `NAMA_PERUSAHAAN` text,
  `ALAMAT_PERUSAHAAN` text,
  `TELP_PERUSAHAAN` varchar(12) DEFAULT NULL,
  `NAMA_WAKIL_PERUSAHAAN` text,
  `TANGGAL_MULAI` date DEFAULT NULL,
  `TANGGAL_SELESAI` date DEFAULT NULL,
  `SURAT_PERSETUJUAN_KP` text,
  `SURAT_PENGUMPULAN_BUKU` text,
  `LEMBAR_NILAI_PERUSAHAAN1` text,
  `LEMBAR_NILAI_PERUSAHAAN2` text,
  `STATUS_PENGAJUAN` varchar(10) DEFAULT NULL,
  `STATUS_KP` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID_KP`),
  KEY `FK_RELATIONSHIP_3` (`NIP`),
  KEY `FK_RELATIONSHIP_4` (`ID_KELOMPOK`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `kp`
--

INSERT INTO `kp` (`ID_KP`, `NIP`, `ID_KELOMPOK`, `NAMA_PERUSAHAAN`, `ALAMAT_PERUSAHAAN`, `TELP_PERUSAHAAN`, `NAMA_WAKIL_PERUSAHAAN`, `TANGGAL_MULAI`, `TANGGAL_SELESAI`, `SURAT_PERSETUJUAN_KP`, `SURAT_PENGUMPULAN_BUKU`, `LEMBAR_NILAI_PERUSAHAAN1`, `LEMBAR_NILAI_PERUSAHAAN2`, `STATUS_PENGAJUAN`, `STATUS_KP`) VALUES
(11, '511111111101', 49, 'PT. Bintang Lima', 'Jl. Rabun Contong 65A - B, Surabaya', '031-5963333', 'Johan J. J', '2015-05-27', '2015-06-24', 'uploads/surat_persetujuan_kp/10932_106167829397339_100000123671294_168303_910661_n.jpg', NULL, 'uploads/lembar_nilai_perusahaan1/keep-calm-and-activate-chair-mode-1.png', NULL, 'Setuju', NULL),
(10, '11111111111111111111', 48, 'PT. Raya Senja', 'Jl. Rabun Contong 65A - B, Surabaya', '031-5888888', 'Henry H. H', '2015-06-04', '2015-07-02', 'uploads/surat_persetujuan_kp/suratPersetujuanKP1.jpg', NULL, NULL, NULL, 'Setuju', NULL),
(9, '511111111102', 47, 'CV. Tunas Gading', 'Jl. Puncak Indah III / 14A, Surabaya', '031-5990567', 'Benny Limbad', '2015-05-30', '2015-06-27', NULL, NULL, NULL, NULL, 'Pending', NULL),
(8, '11111111111111111111', 45, 'PT. Bintang Lima', 'Jl. Kebun Salak 14, Surabaya', '031-5990567', 'Benny Limbad', '2015-05-27', '2015-06-27', 'uploads/surat_persetujuan_kp/2jakqkj3.png', NULL, NULL, NULL, 'Pending', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mhs`
--

DROP TABLE IF EXISTS `mhs`;
CREATE TABLE IF NOT EXISTS `mhs` (
  `NRP` varchar(11) NOT NULL,
  `NAMA` varchar(50) DEFAULT NULL,
  `PASSWORD` text,
  `SKS_TEMPUH` int(11) DEFAULT NULL,
  `ALAMAT` text,
  `TELP` varchar(12) DEFAULT NULL,
  `NILAI_BUKU` smallint(6) DEFAULT NULL,
  `NILAI_LISAN` smallint(6) DEFAULT NULL,
  `NILAI_KENYATAAN` smallint(6) DEFAULT NULL,
  `NILAI_KEDISIPLINAN` smallint(6) DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`NRP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mhs`
--

INSERT INTO `mhs` (`NRP`, `NAMA`, `PASSWORD`, `SKS_TEMPUH`, `ALAMAT`, `TELP`, `NILAI_BUKU`, `NILAI_LISAN`, `NILAI_KENYATAAN`, `NILAI_KEDISIPLINAN`, `STATUS`) VALUES
('5111100112', 'Peter Panyananda H.', 'c0eeb2e4000b5a1145a37a565ba6f3cee063ecc9', 124, 'Jl. Kaca Piring 5, Surabaya', '085755533355', 88, 86, NULL, NULL, NULL),
('5111100074', 'Ebenhaezer W.Y', '6fb668f73e8c230cc70fef7dacf2685ee444e64b', 120, 'Jl. Jojoran 3, Surabaya', '031-5937595', 98, 76, 81, 75, NULL),
('5111100001', 'Alice A. A.', 'a883b47975d12a01d259d95028ca334979505d0c', 100, 'Jl. Durian 1, Surabaya', '031-5996541', NULL, NULL, NULL, NULL, NULL),
('5111100002', 'Bob B. B.', '520891e5f57aab152be52e7c8f00a6e06bb23b5a', 105, 'Jl. Mangga 8, Surabaya', '081266554433', NULL, NULL, NULL, NULL, NULL),
('5111100003', 'Charlie C. C.', '643b64b6137e62d226dc7f205f0d8418e284deb6', 108, 'Jl. Pepaya 9 / 14B, Surabaya', '031-5938393', NULL, NULL, NULL, NULL, NULL),
('5111100004', 'George G. G', 'e23dc23c1505ae4b4037bc6782d8b78f9b11de7c', 150, 'Jl. Jambu Air 73, Sidoarjo', '085755544455', NULL, NULL, NULL, NULL, NULL),
('5111100005', 'Lenny L. L', '280558790e9b4be3ff66eecdf1dcc843d0aace81', 132, 'Jl. Reinburch 31 / 53C, Surabaya', '031-5933939', NULL, NULL, NULL, NULL, NULL),
('5111100006', 'Manny M. M', '9999b540dbbad77988672d8c454ff5e90f616cc7', 123, 'Jl. Manggis 5A, Surabaya', '031-5996541', NULL, NULL, NULL, NULL, NULL),
('5111100007', 'Perry P. P', 'b16a33fe1762fcbde6805079c7e2a32cf25ad57b', 96, 'Jl. Pepaya 10 / 14C, Surabaya', '031-5938394', NULL, NULL, NULL, NULL, NULL),
('5111100008', 'Queen Q. Q', 'fba25a751ea7db6c94af3542f57645cb1cbe73c9', 99, 'Jl. Mangga 6C, Surabaya', '031-5938395', NULL, NULL, NULL, NULL, NULL),
('5111100010', 'Ruben R. R.', '4171f93cac17f0ba701c7abc7e41a3c796debbc0', 98, 'Jl. Jambu Air 74 No 31, Sidoarjo', '085755544466', NULL, NULL, NULL, NULL, NULL),
('5111100009', 'Steven S. S', '2a9e228178f2f8b0d90aee90c39df36b0e3623cb', 102, 'Jl. Durian 3F, Surabaya', '031-5934040', NULL, NULL, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
