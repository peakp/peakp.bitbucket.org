<?php
	class Akun_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
		function change($user,$sha1PasswordBaru){
			$data = array(
				'PASS' => $sha1PasswordBaru,
		   );
			$this->db->where('USER',$user);
			$this->db->update('admin', $data);
		}
		function check($username, $password){
			$this->db->where('user', $username);
			$this->db->where('pass', $password);
			$this->db->limit(1);
			$query = $this->db->get('admin');
            
			if($query->num_rows()==1)
				return 1;
			else
				return 0;
		}
	}
?>