<?php
	class Login_admin_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}
		function checkID($username, $password){
			$this->load->database();
			$this->db->where('user', $username);
			$this->db->where('pass', $password);
			$this->db->limit(1);
			$query = $this->db->get('admin');
			if($query->num_rows()==1)
				return 1;
			else
				return 0;		
		}
	}
?>