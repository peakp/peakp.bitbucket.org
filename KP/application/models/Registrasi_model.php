<?php
	class Registrasi_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}
		function checkNrp($nrp){
			$this->load->database();
			$this->db->where('nrp', $nrp);
			$this->db->limit(1);
			$query = $this->db->get('mhs');
			if($query->num_rows()==1)
				return 1;
			else
				return 0;			
		}
		function insertMhs($nrp,$nama,$password,$sks,$alamat,$telepon){
			$data = array(
				'NRP' => $nrp,
				'NAMA' => $nama,
				'PASSWORD' => $password,
				'SKS_TEMPUH' => $sks,
				'ALAMAT' => $alamat,
				'TELP' => $telepon
		   );
		   $this->db->insert('mhs', $data);
		}
	}
?>