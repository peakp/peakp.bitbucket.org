<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_model extends CI_Model {

	public function __construct()
    {
		parent::__construct();
    }
	
	public function get_mhs_all(){
		$query = $this->db->query('SELECT * FROM MHS');
		return $query->result();
	}
	public function get_mhs_nama_all(){
		$this->db->select('nama');
		$query = $this->db->get('mhs');
		return $query->row_array();
	}
}