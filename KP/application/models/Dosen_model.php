<?php
	class Dosen_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
        function cekNip($nip){
            $this->db->where('NIP', $nip);
            $query = $this->db->get('dosen');
            if($query->num_rows() > 0)
                return 1;
            else
                return 0;
        }
		function get_dosen_all(){
			$query = $this->db->get('dosen');
			return $query->result();
		}
        function get_dosen($nip){
            $this->db->where('nip', $nip);
            $query = $this->db->get('dosen');
            return $query->result();
        }
        function getDosbing(){
			$this->db->where('status_dosen', 'Dosbing-On');
			$this->db->or_where('status_dosen','Dosbing');
			$this->db->order_by('nama_dosen');
			$query = $this->db->get('dosen');
			$return = array();
			if($query->num_rows() > 0) {
				foreach($query->result_array() as $row) {
					$return[$row['NIP']] = $row['NAMA_DOSEN'];
				}
			}
			return $return;
		}
        function update_Dosen($nip,$nama,$telp,$alamat){
            $data = array(
                'nama_dosen' => $nama,
                'telp_dosen' => $telp,
                'alamat_dosen' => $alamat
            );
            $this->db->where('nip', $nip);
            $this->db->update('dosen', $data);
        }
        function updateDosen($nip,$nama,$pass,$telp,$alamat,$status){
			$data = array(
				'NAMA_DOSEN' => $nama,
				'PASSWORD_DOSEN' => $pass,
                'TELP_DOSEN' => $telp,
                'ALAMAT_DOSEN' => $alamat,
                'STATUS_DOSEN' => $status
			);
            $this->db->where('NIP',$nip);
			$this->db->update('DOSEN', $data);
		}
        function insertDosen($nip,$nama,$pass,$telp,$alamat,$status){
            $this->load->database();
			$data = array(
                'NIP' => $nip,
				'NAMA_DOSEN' => $nama,
				'PASSWORD_DOSEN' => $pass,
                'TELP_DOSEN' => $telp,
                'ALAMAT_DOSEN' => $alamat,
                'STATUS_DOSEN' => $status
			);
			$this->db->insert('DOSEN', $data);
        }
        function deleteDosen($nip){
            $this->db->where('nip', $nip);
            $this->db->delete('dosen');
        }
	}
?>