<?php
	class Login_dosbing_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}
		function checkID($username, $password){
			$this->load->database();
			$this->db->where('NIP', $username);
			$this->db->where('PASSWORD_DOSEN', $password);
			$this->db->limit(1);
			$query = $this->db->get('dosen');
            $hasil = $query->result();
            if($query->num_rows()==1){
                if($hasil[0]->STATUS_DOSEN == 'Dosbing')
                    return 1;
                elseif($hasil[0]->STATUS_DOSEN == 'Dosbing-On')
                    return 2;
                elseif($hasil[0]->STATUS_DOSEN == 'Koor')
                    return 3;
            }
			else
				return 0;		
		}
	}
?>