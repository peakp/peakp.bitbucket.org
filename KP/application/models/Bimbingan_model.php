<?php
	class Bimbingan_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
        
        function insertBimbingan($idkp, $topik, $tgl){
			$data = array(
				'TOPIK_BIMBINGAN' => $topik,
				'TANGGAL_BIMBINGAN' => $tgl,
				'ID_KP' =>	$idkp
			);
			$this->db->insert('detail_bimbingan', $data);
        }
        function editBimbingan($idb, $topik, $tgl){
            $data = array(
                'TOPIK_BIMBINGAN' => $topik,
                'TANGGAL_BIMBINGAN' => $tgl,
            );
            $this->db->where('id_detail_bimbingan', $idb);
            $this->db->update('detail_bimbingan', $data);
        }
        function deleteBimbingan($idb){
            $this->db->where('id_detail_bimbingan', $idb);
            $this->db->delete('detail_bimbingan');
        }
		/*function getIdKel($nrp){
			$this->load->database();
			$this->db->where('nrp',$nrp);
			$this->db->or_where('mhs_nrp',$nrp);
			$query = $this->db->get('kelompok')->row();
			return $query->ID_KELOMPOK;
		}
		function checkIdKp($idKelompok){
			$this->load->database();
			$this->db->where('ID_KELOMPOK',$idKelompok);
            //$this->db->where('status_kelompok', 6);
			$query = $this->db->get('kp');
			if($query->num_rows()==0)
				return 0;
			return 1;
		}
		function getIdKp($idKelompok){
			$this->load->database();
			$this->db->where('ID_KELOMPOK',$idKelompok);
			$query = $this->db->get('kp')->row();
			return $query->ID_KP;
		}
		function checkBimbing($getIdKp){
			$this->load->database();
			$this->db->where('ID_KP',$getIdKp);
			$query = $this->db->get('detail_bimbingan');
			if($query->num_rows()==0)
				return 0;
			return 1;
		}*/
		function getAll($getIdKp){
			$this->load->database();
			$this->db->where('ID_KP',$getIdKp);
			$this->db->order_by('tanggal_bimbingan');
			$query = $this->db->get('detail_bimbingan');
			return $query->result();
		}/*
		function checkDetail($idKp,$nomor){
			$this->load->database();
			$this->db->where('ID_KP',$idKp);
			$this->db->where('no',$nomor);
			$query = $this->db->get('detail_bimbingan');
			if($query->num_rows()==0)
				return 0;
			return 1;
		}
		function insertBimbingan($topik,$tanggal,$nomor,$idKp){
			$this->load->database();
			$data = array(
				'TOPIK_BIMBINGAN' => $topik,
				'TANGGAL_BIMBINGAN' => $tanggal,
				'ID_KP' =>	$idKp,
				'no' => $nomor
			);
			$this->db->insert('detail_bimbingan', $data);
		}
		function updateBimbingan($topik,$tanggal,$nomor,$idKp){
			$this->load->database();
			$this->db->where('ID_KP',$idKp);
			$this->db->where('no',$nomor);
			$data = array(
				'TOPIK_BIMBINGAN' => $topik,
				'TANGGAL_BIMBINGAN' => $tanggal
			);
			$this->db->update('detail_bimbingan', $data);
		}*/
	}
?>