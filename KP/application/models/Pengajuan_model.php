<?php
	class Pengajuan_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
		function genIdKp(){
			$this->db->select_max('id_kp');
			$query = $this->db->get('kp')->row();
			return $query->id_kp;
		}
		function getIdKel($nrp, $checkKelType){
			$this->db->where('nrp',$nrp);
			$this->db->or_where('mhs_nrp',$nrp);
            $this->db->where('status_kelompok', $checkKelType);
			$query = $this->db->get('kelompok')->row();
            if($query != NULL)
                return $query->ID_KELOMPOK;
            else
                return NULL;
		}
        function getStatusPengajuan($idKel){
            $this->db->where('id_kelompok', $idKel);
            $this->db->where('status_pengajuan', 'Setuju');
            $query = $this->db->get('kp');
            if($query->num_rows() == 0)
                return 0;
            else
                return 1;
        }
		function getMhs($nrp){
			$this->db->where('nrp',$nrp);
			$query = $this->db->get('mhs')->row();
			return $query;
		}
		function checkKel($idKelompok){
			$this->db->where('ID_KELOMPOK',$idKelompok);
			$query = $this->db->get('kp');
			if($query->num_rows()==0)
				return 0;
			return 1;
		}
        /*function getIdKp($idKelompok){
			$this->db->where('ID_KELOMPOK',$idKelompok);
			$query = $this->db->get('kp');
			if($query->num_rows()==0)
				return 0;
            else{
                $idKp = $query->result();
                $idKp = $idKp[0]->ID_KP;
                
                return $idKp;
            }
		}*/
        function getAllBimbingan($idkp){
            $this->db->where('ID_KP', $idkp);
            $this->db->order_by('tanggal_bimbingan');
            $query = $this->db->get('detail_bimbingan');
            return $query->result();
        }
		function getIdKp($idKelompok){
			$this->db->where('ID_KELOMPOK',$idKelompok);
			$query = $this->db->get('kp')->row();
            if($query != NULL)
                return $query->ID_KP;
            else
                return 0;
		}
		function getAll($idKelompok){
			$this->db->where('ID_KELOMPOK',$idKelompok);
			$query = $this->db->get('kp')->row();
			return $query;
		}
		
		function insertPengajuan($idKelompok,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai){
			$data = array(
				'ID_KELOMPOK' => $idKelompok,
				'NAMA_PERUSAHAAN' => $namaPerusahaan,
				'ALAMAT_PERUSAHAAN' => $alamatPerusahaan,
				'TELP_PERUSAHAAN' => $telpPerusahaan,
				'NAMA_WAKIL_PERUSAHAAN' => $namaWakil,
				'TANGGAL_MULAI' => $tanggalMulai,
				'TANGGAL_SELESAI' => $tanggalSelesai,
                'STATUS_PENGAJUAN' => 'Pending'
		   );
		   $this->db->insert('kp', $data);
		}
		function updatePengajuan($idKp,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai){
			$data = array(
				'NAMA_PERUSAHAAN' => $namaPerusahaan,
				'ALAMAT_PERUSAHAAN' => $alamatPerusahaan,
				'TELP_PERUSAHAAN' => $telpPerusahaan,
				'NAMA_WAKIL_PERUSAHAAN' => $namaWakil,
				'TANGGAL_MULAI' => $tanggalMulai,
				'TANGGAL_SELESAI' => $tanggalSelesai,
                'STATUS_PENGAJUAN' => 'Pending'
		   );
			$this->db->where('ID_KP',$idKp);
			$this->db->update('kp', $data);
		}
	}
?>