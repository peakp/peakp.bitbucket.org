<?php
	class Upload_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
        function setFile($idKel, $path, $filetype){
            $data = array($filetype => $path);
            $this->db->where('id_kelompok', $idKel);
            $this->db->update('kp', $data);
        }
        function getCurrentFile($idKel, $filetype){
            $this->db->where('id_kelompok', $idKel);
            $filetype = $filetype . " AS IMAGE_FILE";
            $this->db->select($filetype, FALSE);
            $query = $this->db->get('kp');
            return $query->result();
        }
        function getCurrentFile2($idKel){
            $this->db->where('id_kelompok', $idKel);
            $filetype = "lembar_nilai_perusahaan1 AS IMAGE_FILE_1, lembar_nilai_perusahaan2 AS IMAGE_FILE_2";
            $this->db->select($filetype, FALSE);
            $query = $this->db->get('kp');
            return $query->result();
        }
    }
?>