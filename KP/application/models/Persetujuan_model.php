<?php
	class Persetujuan_model extends CI_Model{
		public function __construct(){
			parent::__construct();
            $this->load->database();
		}
		function kp_getlist(){
			$this->db->where('status_pengajuan', 'Pending');
			$query = $this->db->get('kp');
			return $query->result();	
		}
		function update_tolak($id_kp){
			$this->db->where('id_kp',$id_kp);
			$data = array(
				'status_pengajuan'=>'Tolak',
			);
			$this->db->update('kp',$data);
            
            $this->db->where('id_kp',$id_kp);
            $this->db->where('status_pengajuan', 'Tolak');
            $idKel = $this->db->get('kp');
            
            $idKel = $idKel->result();
            $idKel = $idKel[0]->ID_KELOMPOK;
            
            $this->db->where('id_kelompok', $idKel);
            $data = array('status_kelompok' => 2);
            $this->db->update('kelompok', $data);
		}
        function update_setuju($id_kp,$nip){
			$this->db->where('id_kp',$id_kp);
			$data = array(
				'status_pengajuan'=>'Setuju',
				'nip'=>$nip
			);
			$this->db->update('kp',$data);
		}
	}
?>