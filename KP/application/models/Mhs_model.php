<?php
	class Mhs_model extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}
        /*cek restriction begin*/
        //cek session
        //cek status -> get_status_kelompok
        //cek pengajuan
        function cekPengajuan($idkel){
            $this->db->where('id_kelompok',$idkel);
            $this->db->where('status_pengajuan', 'Setuju');
            $query = $this->db->get('kp');
            if($query->num_rows() == 0)
                return 0;
            else
                return 1;
        }
        /*cek restriction end*/
		/*get begin*/
		function get_mhs_all(){
			$query = $this->db->get('mhs');
			return $query->result();
		}
		
        function get_kel_all(){
            $sql = "SELECT kpkel.id_kelompok ID_KELOMPOK, kpkel.nama_perusahaan NAMA_PERUSAHAAN,
                kpkel.nrp NRP, m1.nama NAMA1, kpkel.mhs_nrp MHS_NRP, m2.nama NAMA2,
                kpkel.surat_persetujuan_kp SURAT_PERSETUJUAN_KP, kpkel.surat_pengumpulan_buku SURAT_PENGUMPULAN_BUKU,
                kpkel.lembar_nilai_perusahaan1 LEMBAR_NILAI_PERUSAHAAN1, kpkel.lembar_nilai_perusahaan2 LEMBAR_NILAI_PERUSAHAAN2
            FROM (SELECT kp.id_kelompok, kp.nama_perusahaan, kelompok.nrp, kelompok.mhs_nrp,
                kp.surat_persetujuan_kp, kp.surat_pengumpulan_buku,
                kp.lembar_nilai_perusahaan1, kp.lembar_nilai_perusahaan2
                FROM kp LEFT JOIN kelompok ON kp.id_kelompok = kelompok.id_kelompok
                WHERE kp.status_pengajuan = 'Setuju') kpkel
            LEFT JOIN mhs m1 ON kpkel.nrp = m1.nrp
            LEFT JOIN mhs m2 ON kpkel.mhs_nrp = m2.nrp";
            $query = $this->db->query($sql);
			return $query->result();
        }
        
		function get_mhs($nrp){
			$this->db->where('nrp', $nrp);
			$query = $this->db->get('mhs');
			return $query->result();
		}
        
        function get_status_kelompok($nrp){
            //kiri
            $left = 0;
            $right = 0;
            $this->db->where('nrp', $nrp);
            $this->db->select_max('status_kelompok', 'STATUS_KELOMPOK');
            $query = $this->db->get('kelompok');
            $left = $query->result();
            if($left[0]->STATUS_KELOMPOK == NULL) 
                $left = 0;
            else
                $left = $left[0]->STATUS_KELOMPOK;
            //kanan
            $this->db->where('mhs_nrp', $nrp);
            $this->db->select_max('STATUS_KELOMPOK');
            $query = $this->db->get('kelompok');
            $right = $query->result();
            if($right[0]->STATUS_KELOMPOK == NULL) 
                $right = 0;
            else
                $right = $right[0]->STATUS_KELOMPOK;
            //compare
            $hasil = $left;
            if($right > $left) $hasil = $right;
            return $hasil;
        }
        
        function get_status_ditolak($nrp){
            $this->db->where('nrp', $nrp);
            $this->db->where('status_kelompok', 3);
            $query = $this->db->get('kelompok');
            return $query->result();
        }
        
		function get_mhs_kelompok($nrp){
            $this->db->where('nrp', $nrp);
            $this->db->where('status_kelompok', 6);
            $this->db->select('mhs_nrp AS P_NRP');
			$query = $this->db->get('kelompok');
			if( $query->num_rows() != 1 ){
				$this->db->where('mhs_nrp', $nrp);
                $this->db->where('status_kelompok', 6);
                $this->db->select('nrp AS P_NRP', FALSE);
                $query = $this->db->get('kelompok');
			}
            $partner_nrp = $query->result();
            $partner_nrp = $partner_nrp[0]->P_NRP;
            
            $this->db->where('nrp', $partner_nrp);
            $query = $this->db->get('mhs');
            return $query->result();
		}
		
		function get_mhs_available(){
            //SELECT * FROM mhs WHERE
            //nrp NOT IN(SELECT nrp FROM kelompok WHERE status_kelompok = 6 OR status_kelompok = 5 OR status_kelompok = 1)
            //AND
            //nrp NOT IN(SELECT mhs_nrp FROM kelompok WHERE status_kelompok = 6 OR status_kelompok = 5 OR status_kelompok = 1)
			$this->db->where(
                "nrp NOT IN (SELECT nrp FROM kelompok WHERE status_kelompok = 6 OR status_kelompok = 5 OR status_kelompok = 1)", NULL, FALSE);
			$this->db->where(
                "nrp NOT IN (SELECT mhs_nrp FROM kelompok WHERE status_kelompok = 6 OR status_kelompok = 5 OR status_kelompok = 1)", NULL, FALSE);
			$query = $this->db->get('mhs');
			if($query->num_rows() > 0){
				return $query->result();
			}
			else{
				return 0;
			}
		}
        function get_status_pengajuan($idKel){
            $this->db->where('id_kelompok', $idKel);
            $query = $this->db->get('kp');
            return $query->result();
        }
        function get_mhs_invited($nrp){
            //SELECT mhs.nrp nrp, mhs.nama nama FROM kelompok INNER JOIN mhs ON mhs.nrp = kelompok.mhs_nrp
            //WHERE nrp = 5111100002 AND status_kelompok = 4
            $this->db->select('mhs.nrp AS NRP, mhs.nama AS NAMA', FALSE);
            $this->db->from('mhs');
            $this->db->join('kelompok', 'mhs.nrp = kelompok.mhs_nrp');
            $this->db->where('kelompok.nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $query = $this->db->get();
            return $query->result();
        }
        
        function get_mhs_inviting($nrp){
            $this->db->select('mhs.nrp AS NRP, mhs.nama AS NAMA', FALSE);
            $this->db->from('mhs');
            $this->db->join('kelompok', 'mhs.nrp = kelompok.nrp');
            $this->db->where('kelompok.mhs_nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $query = $this->db->get();
            return $query->result();
        }
		/*get end*/
		/*insert begin*/
		function insert_kelompok($nrp, $nrp_teman){
			$data = array(
				'nrp' => $nrp,
				'mhs_nrp' => $nrp_teman,
				'status_kelompok' => 4
			);
			$this->db->insert('kelompok', $data);
		}
        
        function insert_kp_sendiri($nrp){
            //insert
            $data = array(
                'nrp' => $nrp,
                'mhs_nrp' => $nrp,
                'status_kelompok' => 5
            );
            $this->db->insert('kelompok', $data);
            //update
            $data = array('status_kelompok' => 3);
            $this->db->where('mhs_nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $this->db->update('kelompok', $data);
        }
		/*insert end*/
        /*update begin*/
        function updateMhs($nrp,$nama,$sks,$telp,$alamat){
            $data = array(
                'nama' => $nama,
                'sks_tempuh' => $sks,
                'telp' => $telp,
                'alamat' => $alamat
            );
            $this->db->where('nrp', $nrp);
            $this->db->update('mhs', $data);
        }
        
        function update_accept_kelompok($nrp, $nrp_teman){
            $data = array('status_kelompok' => 6);
            $this->db->where('nrp', $nrp_teman);
            $this->db->where('mhs_nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $this->db->update('kelompok', $data);
            
            $data = array('status_kelompok' => 3);
            $this->db->where('mhs_nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $this->db->update('kelompok', $data);
        }
        
        function update_tolak_kelompok($nrp, $nrp_teman){
            $data = array('status_kelompok' => 3);
            $this->db->where('nrp', $nrp_teman);
            $this->db->where('mhs_nrp', $nrp);
            $this->db->where('status_kelompok', 4);
            $this->db->update('kelompok', $data);
        }
        
        function update_pisah_kelompok($nrp, $status){
            if($status != 6){
                $data = array('status_kelompok' => 2);
                $this->db->where('nrp', $nrp);
                $this->db->where('status_kelompok', $status);
                $this->db->update('kelompok', $data);
            }
            else{
                $this->db->where('nrp', $nrp);
                $this->db->where('status_kelompok', 6);
                $query = $this->db->get('kelompok');
                $col = 'nrp';
                if($query->num_rows < 1){
                    $col = 'mhs_nrp';
                }
                
                $data = array('status_kelompok' => 2);
                $this->db->where($col, $nrp);
                $this->db->where('status_kelompok', 6);
                $this->db->update('kelompok', $data);
            }
        }
        /*update end*/
	}
?>