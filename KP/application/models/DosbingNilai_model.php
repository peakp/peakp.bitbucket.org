<?php
	class DosbingNilai_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}
		function getKpBimbing($nip){
			$this->load->database();
			$sql = "select kelompok.NRP, kelompok.MHS_NRP, kelompok.ID_KELOMPOK, NAMA_PERUSAHAAN, NILAI_LISAN, NILAI_BUKU from kp,mhs, kelompok where kelompok.ID_KELOMPOK = kp.ID_KELOMPOK AND nip = ? and (kelompok.MHS_NRP = mhs.nrp or kelompok.NRP = mhs.nrp )";
			$query = $this->db->query($sql, array($nip));
			return $query->result();
		}
        function getKpBimbing2(){
			$this->load->database();
			$sql = "select kelompok.NRP, kelompok.MHS_NRP, kelompok.ID_KELOMPOK, NAMA_PERUSAHAAN, NILAI_LISAN, NILAI_BUKU from kp,mhs, kelompok where kelompok.ID_KELOMPOK = kp.ID_KELOMPOK and kp.status_pengajuan = 'Setuju' and(kelompok.MHS_NRP = mhs.nrp or kelompok.NRP = mhs.nrp )";
			$query = $this->db->query($sql);
			return $query->result();
		}
		function updateNilai($nrp,$nilaiLisan,$nilaiKp){
			$this->load->database();
			$data = array(
				'nilai_buku' => $nilaiKp,
				'nilai_lisan' => $nilaiLisan
			);
			$this->db->where('Nrp',$nrp);
			$this->db->update('mhs', $data);
		}
        function getAllMhs(){
            $this->load->database();
            $sql = "select kelompok.NRP, kelompok.MHS_NRP, kelompok.ID_KELOMPOK, NAMA_PERUSAHAAN, NILAI_KENYATAAN, NILAI_KEDISIPLINAN from kp,mhs, kelompok where kelompok.ID_KELOMPOK = kp.ID_KELOMPOK and (kelompok.MHS_NRP = mhs.nrp or kelompok.NRP = mhs.nrp ) and status_pengajuan = 'Setuju'";
            $query = $this->db->query($sql);
			return $query->result();
        }
        function updateNilai2($nrp,$nilaiKenyataan,$nilaiKedisiplinan){
			$this->load->database();
			$data = array(
				'nilai_kenyataan' => $nilaiKenyataan,
				'nilai_kedisiplinan' => $nilaiKedisiplinan
			);
			$this->db->where('Nrp',$nrp);
			$this->db->update('mhs', $data);
		}
	}
?>