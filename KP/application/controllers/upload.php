<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
    
    function view_template($current_view, $data){
		$this->load->view('header');
		$this->load->view($current_view, $data);
		return NULL;
	}
    
	function viewForm1($filetype){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($filetype == 'surat_pengumpulan_buku')
            if($this->cekRestriksiPengajuan() == 0)
                return;
        
        $data['filetp'] = $filetype;
        $nrp = $this->session->userdata('noid');
		$this->load->model('Mhs_model');
        $this->load->model('Pengajuan_model');
        $this->load->model('Upload_model');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        $idKel = $this->Pengajuan_model->getIdKel($nrp, $kelType);
        $data['current_file'] = $this->Upload_model->getCurrentFile($idKel, $filetype);
        $this->view_template('Upload1_view', $data);
	}
    
    function viewForm2(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
        $filetype = "lembar_nilai_perusahaan";
        $data['filetp'] = $filetype;
        $nrp = $this->session->userdata('noid');
		$this->load->model('Mhs_model');
        $this->load->model('Pengajuan_model');
        $this->load->model('Upload_model');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        $idKel = $this->Pengajuan_model->getIdKel($nrp, $kelType);
        $data['current_file'] = $this->Upload_model->getCurrentFile2($idKel);
        $this->view_template('Upload2_view', $data);
	}

	function do_upload()
	{
        $nrp = $this->session->userdata('noid');
        $filetype = $this->input->post("filetype");
        
		$config['upload_path'] = './uploads/' . $filetype . '/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2048';
        
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$this->view_template('Upload1_view', $error);
		}
		else
		{
			$data = $this->upload->data();
            $this->load->model('Mhs_model');
            $this->load->model('Pengajuan_model');
            $this->load->model('Upload_model');
            $kelType = $this->Mhs_model->get_status_kelompok($nrp);
            $idKel = $this->Pengajuan_model->getIdKel($nrp, $kelType);
            $path = "uploads/".$filetype."/".$data['file_name'];
            $this->Upload_model->setFile($idKel, $path, $filetype);
            if($filetype == 'surat_persetujuan_kp')
                redirect('upload/viewForm1/surat_persetujuan_kp','refresh');
            elseif($filetype == 'surat_pengumpulan_buku')
                redirect('upload/viewForm1/surat_pengumpulan_buku','refresh');
            else
                redirect('upload/viewForm2','refresh');
		}
	}
    
    /*cek restriksi begin*/
    function cekLoginUser(){
        if($this->session->userdata('user')){
            //$GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->home();
            return 0;
        }
        else{
            return 1;
        }
    }
    function cekRestriksiUser(){
        if(!$this->session->userdata('user')){
            $GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->login();
            return 0;
        }
        else{
            return 1;
        }
    }
    function cekRestriksiKelompok(){
        $this->load->model('Mhs_model');
        $nrp = $this->session->userdata('noid');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        if($kelType != 5 && $kelType != 6){
            $GLOBALS['err_msg'] = "Anda harus memiliki kelompok untuk mengakses halaman ini!";
            $this->view_template('Home','');
            return 0;
        }
        else
            return 1;
    }
    function cekRestriksiPengajuan(){
        $this->load->model('Mhs_model');
        $this->load->model('Pengajuan_model');
        $nrp = $this->session->userdata('noid');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        $idKel = $this->Pengajuan_model->getIdKel($nrp, $kelType);
        $cekPengajuan = $this->Pengajuan_model->getStatusPengajuan($idKel);
        if($cekPengajuan == 0){
            $GLOBALS['err_msg'] = "Formulir Pengajuan Anda harus disetujui untuk mengakses halaman ini!";
            $this->view_template('Home','');
            return 0;
        }
        else
            return 1;
    }
    /*cek restriksi end*/
}
?>