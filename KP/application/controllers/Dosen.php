<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}
    function view_template($current_view, $data){
		$this->load->view('header');
		$this->load->view($current_view, $data);
		return NULL;
	}
    function home(){
		$this->view_template('Home','');
	}
	function main(){
        if($this->session->userdata('user'))
            $this->home();
        else
            $this->view_template('Login_dosbing_view', '');
	}
    
    function akun(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nip = $this->session->userdata('noid');
        $this->load->model('Dosen_model');
        $data['dosen_data'] = $this->Dosen_model->get_dosen($nip);
        $this->view_template('Dosen_akun_view',$data);
    }
    
    function editProfile(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Dosen_model');
		$nip = $this->session->userdata('noid');
		$nama = $this->input->post('nama1');
        $telp = $this->input->post('telp1');
        $alamat = $this->input->post('alamat1');
		$this->Dosen_model->update_Dosen($nip,$nama,$telp,$alamat);
		redirect('Dosen/akun', 'refresh');
    }
    
	function submit(){
		$this->load->model('Login_dosbing_model');			
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = sha1($password);
		$check = $this->Login_dosbing_model->checkID($username,$password);
		$data['check'] = $check; 
		if($check == 1){
            $this->session->set_userdata('user', 'dosbing');
            $this->session->set_userdata('noid', $username);
            redirect('Dosen/home', 'refresh');
        }
        elseif($check == 2){
            $this->session->set_userdata('user', 'dosbing');
            $this->session->set_userdata('noid', $username);
            redirect('Dosen/home', 'refresh');
        }
        elseif($check == 3){
            $this->session->set_userdata('user', 'koor');
            $this->session->set_userdata('noid', $username);
            redirect('Dosen/home', 'refresh');
        }
		else{
            $GLOBALS['err_msg'] = "Username / Password salah!";
            $this->main();
            //$this->session->set_flashdata('message', 'Added to cart');
		}
	}
	function mainNilai(){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$nip = $this->session->userdata('noid');
		$this->load->library('table');
		$this->load->helper('html');
		$this->load->model('DosbingNilai_model');
		$data['query'] = $this->DosbingNilai_model->getKpBimbing($nip);
		$this->view_template('DosbingNilai_view', $data);
	}
    function mainNilai2(){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$nip = $this->session->userdata('noid');
		$this->load->library('table');
		$this->load->helper('html');
		$this->load->model('DosbingNilai_model');
		$data['query'] = $this->DosbingNilai_model->getKpBimbing2();
		$this->view_template('DosbingNilai_view', $data);
	}
    function KoorNilai(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nip = $this->session->userdata('noid');
		$this->load->library('table');
		$this->load->helper('html');
		$this->load->model('DosbingNilai_model');
        $data['query'] = $this->DosbingNilai_model->getAllMhs();
		$this->view_template('KoorNilai_view', $data);
    }
	function submitNilai($nrp){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$nip = $this->session->userdata('noid');
		$nilaiLisan = $this->input->get('nilaiLisan');
		$nilaiKp = $this->input->get('nilaiKp');
		$this->load->model('DosbingNilai_model');
		$this->DosbingNilai_model->updateNilai($nrp,$nilaiLisan,$nilaiKp);		
		redirect('Dosen/mainNilai', 'refresh');
	}
    function submitNilai2($nrp){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$nip = $this->session->userdata('noid');
		$nilaiKenyataan = $this->input->get('nilaiKenyataan');
		$nilaiKedisiplinan = $this->input->get('nilaiKedisiplinan');
		$this->load->model('DosbingNilai_model');
		$this->DosbingNilai_model->updateNilai2($nrp,$nilaiKenyataan,$nilaiKedisiplinan);		
		redirect('Dosen/KoorNilai', 'refresh');
	}
    /*Persetujuan begin*/
    function persetujuan(){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$this->load->library('table');
		$this->load->helper('html');
		$this->load->model('Persetujuan_model');
		$this->load->model('Dosen_model');
		$data['dosbing']= $this->Dosen_model->getDosbing();
		$data['query'] = $this->Persetujuan_model->kp_getlist();
		$this->view_template('Persetujuan_view',$data);
	}
    function setuju($id_kp){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nip = $this->input->post('dosbing_id');	
		$this->load->model('Persetujuan_model');
		$this->Persetujuan_model->update_setuju($id_kp,$nip);
		redirect('Dosen/persetujuan', 'refresh');
    }
	function tolak($id_kp){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Persetujuan_model');
		$this->Persetujuan_model->update_tolak($id_kp);
		redirect('Dosen/persetujuan', 'refresh');
    }
    /*Persetujuan end*/
    /*Rekap begin*/
    function rekap(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Mhs_model');
        $data['mhs_all'] = $this->Mhs_model->get_mhs_all();
        $this->view_template('RekapNilai_view', $data);
    }
    function rekapDokumen(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Mhs_model');
        $data['kel_all'] = $this->Mhs_model->get_kel_all();
        $this->view_template('RekapDokumen_view', $data);
    }
    /*Rekap end*/
    /*cek restriksi begin*/
    function cekRestriksiUser(){
        if(!$this->session->userdata('user')){
            $GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->main();
            return 0;
        }
        else{
            return 1;
        }
    }
    function cekLoginUser(){
        if($this->session->userdata('user')){
            //$GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->home();
            return 0;
        }
        else{
            return 1;
        }
    }
    /*cek restriksi end*/
}