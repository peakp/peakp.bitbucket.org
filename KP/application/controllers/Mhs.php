<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhs extends CI_Controller{
	
    public $err_msg = "";
    
	public function __construct(){
		parent::__construct();
	}
    function index(){
        $this->home();
    }
	function view_template($current_view, $data){
		$this->load->view('header');
		$this->load->view($current_view, $data);
	}
	function home(){
		$this->view_template('Home','');
	}
	
	/*mhs akun begin*/
	//mhs1 - current mhs
	//mhs2 - mhs partner
	//status - NULL, Self, Pair, Denied, Invite, Invited, Delete, Done
    //no Delete & Done
	//left => Invite / Pair / Self / Denied
	//right => Invited / Pair / Self / Denied
    //PRIORITY
    //Pair/Self > Invite/Invited    > Denied    > Delete   > Done
    //6/5       > 4(left)/4(right)  > 3         > 2        > 1
	// INFO Denied BUAT QUERY Update!!!
    function akun(){                /*DEFAULT*/
        if($this->cekRestriksiUser() == 0)
            return;
        
		$this->load->model('Mhs_model');
        $nrp = $this->session->userdata('noid');
        $data['mhs_data'] = $this->Mhs_model->get_mhs($nrp);
        $data['mhs_status'] = $this->Mhs_model->get_status_kelompok($nrp);
        if($data['mhs_status'] == 6)
            $data['mhs_partner'] = $this->Mhs_model->get_mhs_kelompok($nrp);
        elseif($data['mhs_status'] == 5)
            $data['mhs_partner'] = NULL;
        elseif($data['mhs_status'] != 0){
            $data['mhs_invited'] = $this->Mhs_model->get_mhs_invited($nrp);
            if($data['mhs_invited'] == NULL){
                $cekTolak = $this->Mhs_model->get_status_ditolak($nrp);
                if($cekTolak != NULL)
                    $data['mhs_status'] = 3;
            }
            //$data['mhs_inviting'] = $this->Mhs_model->get_mhs_inviting($nrp);
        }
        $this->load->model('Pengajuan_model');
        $idKel = $this->Pengajuan_model->getIdKel($nrp, $data['mhs_status']);
        if($idKel != NULL)
            $data['mhs_pengajuan'] = $this->Mhs_model->cekPengajuan($idKel);
        //echo $data['mhs_pengajuan'];
		$this->view_template('Mhs_akun_view', $data);
	}
    function editProfile(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Mhs_model');
		$nrp = $this->session->userdata('noid');
		$nama = $this->input->post('nama1');
        $sks = $this->input->post('sks1');
        $telp = $this->input->post('telp1');
        $alamat = $this->input->post('alamat1');
		$this->Mhs_model->updateMhs($nrp,$nama,$sks,$telp,$alamat);
		redirect('Mhs/akun', 'refresh');
    }
	function pisah_kelompok($status){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nrp = $this->session->userdata('noid');
        $this->load->model('Mhs_model');
        $this->Mhs_model->update_pisah_kelompok($nrp, $status);
        redirect('Mhs/akun', 'refresh');
	}
	/*mhs akun end*/
	/*mhs invite begin*/
    function invite(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $this->load->model('Mhs_model');
        $nrp = $this->session->userdata('noid');
        $data['mhs_invited'] = $this->Mhs_model->get_mhs_invited($nrp);
        if($data['mhs_invited'] == NULL){
            $this->load->model('Mhs_model');
            $data['mhs_available'] = $this->Mhs_model->get_mhs_available();
            $data['mhs_inviting'] = $this->Mhs_model->get_mhs_inviting($nrp);
            $this->view_template('Mhs_invite_view',$data);
        }
        else
            redirect('Mhs/akun', 'refresh');
	}
    
    function kp_sendiri(){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nrp = $this->session->userdata('noid');
        $this->load->model('Mhs_model');
        $this->Mhs_model->insert_kp_sendiri($nrp);
        redirect('Mhs/akun', 'refresh');
    }
	
	function invite_teman($nrp_teman){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$this->load->model('Mhs_model');
		$this->Mhs_model->insert_kelompok($this->session->userdata('noid'), $nrp_teman);
		redirect('Mhs/akun','refresh');
	}
	
	function accept_teman($nrp_teman){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nrp = $this->session->userdata('noid');
		$this->load->model('Mhs_model');
		$this->Mhs_model->update_accept_kelompok($nrp, $nrp_teman);
		redirect('Mhs/akun','refresh');
	}
    
    function tolak_teman($nrp_teman){
        if($this->cekRestriksiUser() == 0)
            return;
        
        $nrp = $this->session->userdata('noid');
        $this->load->model('Mhs_model');
        $this->Mhs_model->update_tolak_kelompok($nrp, $nrp_teman);
        redirect('Mhs/invite','refresh');
    }
	/*mhs invite end*/
    /*pengajuan begin*/
    // fitur Pengajuan KP
    function pengajuan(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0){
            return;
        }
		$nrp = $this->session->userdata('noid');
		$this->load->model('Pengajuan_model');
        $this->load->model('Mhs_model');
        $checkKelType = $this->Mhs_model->get_status_kelompok($nrp);
		// get Id Kelompok
		$idKelompok = $this->Pengajuan_model->getIdKel($nrp, $checkKelType);
		// cek apakah id Kelompok sudah melakukan pengajuan KP
		$checkKel = $this->Pengajuan_model->checkKel($idKelompok);
		// checkKel = 0, belum buat pengajuan, textbox kosong
		// else textbox ad isinya
		if($checkKel == 0){
            $data['namaPerusahaan'] = null;
			$data['alamatPerusahaan'] = null;
			$data['telpPerusahaan'] = null;
			$data['namaWakil'] = null;
			$data['tanggalMulai'] = null;
			$data['tanggalSelesai'] = null;			
			$data['check'] = 0;
			$this->view_template('Pengajuan_view', $data);
        }
		else{
            $getAll = $this->Pengajuan_model->getAll($idKelompok);
            $data['namaPerusahaan'] = $getAll->NAMA_PERUSAHAAN;
            $data['alamatPerusahaan'] = $getAll->ALAMAT_PERUSAHAAN;
            $data['telpPerusahaan'] = $getAll->TELP_PERUSAHAAN;
            $data['namaWakil'] = $getAll->NAMA_WAKIL_PERUSAHAAN;
            $data['tanggalMulai'] = $getAll->TANGGAL_MULAI;
            $data['tanggalSelesai'] = $getAll->TANGGAL_SELESAI;	
            $data['check'] = 1;
            $this->view_template('Pengajuan_view', $data);
        }	
    }
    
    function submitPengajuan(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
		// if check = 1, data suda ada, update
		// else insert
        $this->load->model('Mhs_model');
		$this->load->model('Pengajuan_model');
		$nrp = $this->session->userdata('noid');
		// get Id Kelompok
        $checkKelType = $this->Mhs_model->get_status_kelompok($nrp);
		$idKelompok = $this->Pengajuan_model->getIdKel($nrp, $checkKelType);
		// get Sks tempuh
		$getMhs = $this->Pengajuan_model->getMhs($nrp);
		$mhs['nama'] = $getMhs->NAMA;
		$mhs['nrp'] = $getMhs->NRP;
		$mhs['sks'] = $getMhs->SKS_TEMPUH;
		$mhs['alamat'] = $getMhs->ALAMAT;
		$mhs['telepon'] = $getMhs->TELP;
        
        $namaPerusahaan = $this->input->post('namaPerusahaan');
        $alamatPerusahaan = $this->input->post('alamatPerusahaan');
        $telpPerusahaan = $this->input->post('telpPerusahaan');
        $namaWakil = $this->input->post('namaWakil');
        $tanggalMulai = $this->input->post('tanggalMulai');
        $tanggalSelesai = $this->input->post('tanggalSelesai');
        
        $idKp = $this->Pengajuan_model->getIdKp($idKelompok);
        if($idKp == 0){
            if(isset($_POST["save"])){
                $this->Pengajuan_model->insertPengajuan($idKelompok,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
                redirect('Mhs/pengajuan', 'refresh');
            }
            elseif(isset($_POST["print"])){
                $this->Pengajuan_model->insertPengajuan($idKelompok,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
                $this->create_pdf($mhs, $namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
            }
        }
        else{
            if(isset($_POST["save"])){
                $this->Pengajuan_model->updatePengajuan($idKp,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
                redirect('Mhs/pengajuan', 'refresh');
            }
            elseif(isset($_POST["print"])){
                $this->Pengajuan_model->updatePengajuan($idKp,$namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
                $this->create_pdf($mhs, $namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai);
            }
        }
    }
    
    function create_pdf($mhs, $namaPerusahaan,$alamatPerusahaan,$telpPerusahaan,$namaWakil,$tanggalMulai,$tanggalSelesai){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        
        $this->load->library("Pdf");
        $nama = $mhs['nama'];
        $nrp = $mhs['nrp'];
        $sks = $mhs['sks'];
        $alamat = $mhs['alamat'];
        $telp = $mhs['telepon'];
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Persetujuan KP');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
        $pdf->setFooterData(array(0,64,0), array(0,64,128)); 

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
			
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }   

        // ---------------------------------------------------------    

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);   

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);   

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage(); 

        // set text shadow effect
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
        // Set some content to print
        $html = <<<EOD
        <h1 style ="font-size: 20px;font-face:'Arial';text-align:center;">Formulir Pengajuan Kerja Praktek 	<br/> </h1>
            <table style="width:100%">
				<tr style>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Nama Mahasiswa</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $nama	 </td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">NRP</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $nrp</td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Sks Yang Telah Ditempuh</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $sks </td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Alamat Surat</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $alamat</td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Nomor Telepon</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $telp</td> 
				</tr>
			</table>
			<br/>
			<h1 style ="font-size: 14px;font-face:'Arial';text-align:center;"><i>Akan Melakukan Kerja Praktek di: </i> <br/></h1>
			<br/>
			<table style="width:100%">
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Nama Instansi / Perusahaan</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $namaPerusahaan</td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Alamat Perusahaan</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $alamatPerusahaan </td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Nomor Telepon</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $telpPerusahaan </td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Nama Wakil Instansi/Perusahaan</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $namaWakil </td> 
				</tr>
				<tr>
					<td style ="padding-right: 10px;width:33%;font-size: 12px;font-face:'Arial'">Rencana Tanggal Kerja Praktek</td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $tanggalMulai / $tanggalSelesai </td> 
				</tr>
			</table>
			<br/> <br/><br/> <br/>
			<table style="width:100%">
				<tr>
					<td style="width:30%"> &nbsp; </td>
					<td style="width:30%"> &nbsp; </td>
					<td style="font-size: 14px;font-face:'Arial';"> Surabaya, <br/> </td> 
				</tr>
				<tr>
					<td style="width:30%;font-size: 14px;font-face:'Arial';"> Mengetahui Dosen Wali, <br/> <br/> <br/> <br/><br/></td>
					<td style="width:30%"> &nbsp; </td>
					<td style="font-size: 14px;font-face:'Arial'"> Pemohon, </td> 
				</tr>
				<tr>
					<td style="width:30%;font-size: 14px;font-face:'Arial';">_________________________ <br/> <br/> <br/></td>
					<td style="width:30%"> &nbsp; </td>			
					<td style="font-size: 14px;font-face:'Arial'"> $nama </td> 
				</tr>
				<tr>
					<td style="width:30%"> &nbsp; </td>		
					<td style="width:30%;font-size: 14px;font-face:'Arial';text-align:center">Menyetujui,</td>
					<td > &nbsp; </td> 
				</tr>
				<tr>
					<td style="width:25%"> &nbsp; </td>
					<td style="width:40%;font-size: 14px;font-face:'Arial';text-align:center">Koordinator Kerja Praktek, <br/> <br/> <br/><br/> <br/></td>
					<td > &nbsp; </td> 
				</tr>
				<tr>
					<td style="width:25%"> &nbsp; </td>
					<td style="width:40%;text-align:center">_________________________</td>	
					<td > &nbsp; </td> 
				</tr>		
			</table>
			<br/><br/><br/><br/><br/>
			<p style="font-size: 14px;font-face:'Arial'"> *) Tiap Mahasiswa Mengisi 1 Lembar Formulir </p>
EOD;

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   

        // ---------------------------------------------------------    

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output('example_001.pdf', 'I');   
    }
    /*pengajuan end*/
    /*bimbingan begin*/
    function bimbingan(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
        $nrp = $this->session->userdata('noid');
        $this->load->model('Mhs_model');
        $this->load->model('Pengajuan_model');
        $checkKelType = $this->Mhs_model->get_status_kelompok($nrp);
		// get Id Kelompok
		$idKelompok = $this->Pengajuan_model->getIdKel($nrp, $checkKelType);
		$data['idkp'] = $this->Pengajuan_model->getIdKp($idKelompok);
        $data['bimbingan_all'] = $this->Pengajuan_model->getAllBimbingan($data['idkp']);
        if($data['idkp'] == 0){
            $GLOBALS['err_msg'] = "Error!";
            $this->home();
        }
        else{
            $this->view_template('BimbinganDosen_view', $data);
        }
    }
    
    function insertBimbingan(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
        $idkp = $this->input->post('idkp1');
        $topik = $this->input->post('topik1');
        $tgl = $this->input->post('tgl1');
        
        $tgl2 = explode("/", $tgl);
        $tgl = $tgl2[2]."-".$tgl2[0]."-".$tgl2[1];
        
        $nrp = $this->session->userdata('noid');
        $this->load->model('Bimbingan_model');
        $this->Bimbingan_model->insertBimbingan($idkp, $topik, $tgl);
        redirect('Mhs/bimbingan','refresh');
    }
    
    function editBimbingan(){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
        $idb = $this->input->post('idb1');
        $topik = $this->input->post('topik1');
        $tgl = $this->input->post('tgl1');
        //mm/dd/yyyy
        //yyyy-mm-dd
        $tgl2 = explode("/", $tgl);
        $tgl = $tgl2[2]."-".$tgl2[0]."-".$tgl2[1];
        //echo $tgl;
        $nrp = $this->session->userdata('noid');
        $this->load->model('Bimbingan_model');
        $this->Bimbingan_model->editBimbingan($idb, $topik, $tgl);
        redirect('Mhs/bimbingan','refresh');
    }
    
    function deleteBimbingan($idb){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
        $this->load->model('Bimbingan_model');
        $this->Bimbingan_model->deleteBimbingan($idb);
        redirect('Mhs/bimbingan','refresh');
    }
    
	function pdfBimbing($idkp){
        if($this->cekRestriksiUser() == 0)
            return;
        if($this->cekRestriksiKelompok() == 0)
            return;
        if($this->cekRestriksiPengajuan() == 0)
            return;
        
		$nrp = $this->session->userdata('noid');
		$this->load->model('Bimbingan_model');
		$getAll = $this->Bimbingan_model->getAll($idkp);
        $i = 1;
		foreach($getAll as $row){
            //$dataNomor[$row->no] = $row->no;
            $dataNomor[$i] = $i;
			$dataTopik[$i] = $row->TOPIK_BIMBINGAN;
			$dataTanggal[$i] = $row->TANGGAL_BIMBINGAN;
            $i++;
        }
        for($i; $i<9; $i++){
            $dataNomor[$i] = $i;
			$dataTopik[$i] = "";
			$dataTanggal[$i] = "";
        }
			// load database untuk mencari nama da
		$this->load->model('Pengajuan_model');
		$getMhs = $this->Pengajuan_model->getMhs($nrp);
		$mhs['nama'] = $getMhs->NAMA;
		$nama = $mhs['nama'];
		$this->load->library("Pdf");
			 // create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    

			// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetTitle('Bimbingan KP');


			// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
		$pdf->setFooterData(array(0,64,0), array(0,64,128)); 

			// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  

			// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 

			// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    

			// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 

			// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  

			// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}   

			// ---------------------------------------------------------    

			// set default font subsetting mode
		$pdf->setFontSubsetting(true);   

			// Set font
			// dejavusans is a UTF-8 Unicode font, if you only need to
			// print standard ASCII chars, you can use core fonts like
			// helvetica or times to reduce file size.
		$pdf->SetFont('dejavusans', '', 14, '', true);   

			// Add a page
			// This method has several options, check the source code documentation for more information.
		$pdf->AddPage(); 

			// set text shadow effect
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
			// Set some content to print
		$html = <<<EOD
			<h1 style ="font-size: 20px;font-face:'Arial';text-align:center;">Bimbingan Kerja Praktek 	<br/> </h1>
			<table style="width:100%">
				<tr style>
					<td style ="width:13%;font-size: 12px;font-face:'Arial'">Nama </td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $nama	 </td> 
				</tr>
				<tr style>
					<td style ="width:13%;font-size: 12px;font-face:'Arial'">NRP </td>
					<td style ="font-size: 12px;font-face:'Arial'"> : $nrp	 </td> 
				</tr>
			</table>
			<br/>
			<br/>
			<br/>
			<br/>
			<table style="width:100%;" border="2" cellpadding="5" cellspacing="0">
				<thead>
					<tr>
						<th style ="width:5%;font-size: 13px;font-face:'Arial';border:0px" align="center">NO</th>
						<th style ="width:70%;font-size: 13px;font-face:'Arial';border:0px" align="center">TOPIK BIMBINGAN</th>    
						<th style ="width:25%;font-size: 13px;font-face:'Arial';border:0px" align="center">TANGGAL BIMBINGAN</th>     
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[1]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[1]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[1]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[2]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[2]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[2]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[3]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[3]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[3]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[4]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[4]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[4]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[5]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[5]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[5]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[6]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[6]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[6]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[7]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[7]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[7]</td>     
					</tr>
					<tr>
						<td style ="width:5%;font-size: 12px;font-face:'Arial'" align="center">$dataNomor[8]</td>
						<td style ="width:70%;font-size: 12px;font-face:'Arial'">$dataTopik[8]</td>    
						<td style ="width:25%;font-size: 12px;font-face:'Arial'">$dataTanggal[8]</td>     
					</tr>
				</tbody>
			</table>
EOD;

			// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   

			// ---------------------------------------------------------    

			// Close and output PDF document
			// This method has several options, check the source code documentation for more information.
        $pdf->Output('bimbingan.pdf', 'I'); 
    }
    /*bimbingan end*/
    /**/
    function login(){
        if($this->cekLoginUser() == 0)
            return;
        $this->load->helper(array('form'));
		$this->view_template('Login_view','');
    }
	function logout(){
        $data = array(
            'noid' => '',
            'user' => '',
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('Mhs/home','refresh');
	}
    function submit(){
		$this->load->model('Login_model');		
		$this->load->driver('session');
		$username = $this->input->post('username');
		$password = sha1($this->input->post('password'));
		$check = $this->Login_model->checkID($username,$password);
		$data['check'] = $check; 
		if($check == 1){
            $this->session->set_userdata('user', 'mhs');
            $this->session->set_userdata('noid', $username);
			redirect('/Mhs/home');
        }
		else{
            $GLOBALS['err_msg']="Username / Password Anda salah!";
			$this->login();
        }
    }
    function register(){
        if($this->cekLoginUser() == 0)
            return;
		//$this->load->view('Register_view');
        $this->view_template('Register_view', '');
	}
	function submitRegister(){
		$this->load->model('Registrasi_model');
		$nrp = $this->input->post('nrp');
		$nama = $this->input->post('nama');
		$password = $this->input->post('password');
		$confirm = $this->input->post('confirm');
		$sks = $this->input->post('sks');
		$alamat = $this->input->post('alamat');
		$telepon = $this->input->post('telepon');
		// cek password dan confrim password
		if($password != $confirm){
            $GLOBALS['err_msg'] = "Password tidak sama!";
            $this->register();
            return;
		}
		// cek nrp, 1->sudah ada nrpnya, 0 -> belum ada, insert
        $check = $this->Registrasi_model->checkNrp($nrp);
		if($check == 1){
            $GLOBALS['err_msg']= "NRP sudah terdaftar!";
			$this->register();
        }
		else{
            $sha1Password = sha1($password);
            //$sha1Password = $password;
			$this->Registrasi_model->insertMhs($nrp,$nama,$sha1Password,$sks,$alamat,$telepon);
            redirect('Mhs/home','refresh');
        }
    }
    /**/
    /*cek restriksi begin*/
    function cekLoginUser(){
        if($this->session->userdata('user')){
            //$GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->home();
            return 0;
        }
        else{
            return 1;
        }
    }
    function cekRestriksiUser(){
        if(!$this->session->userdata('user')){
            $GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->login();
            return 0;
        }
        else{
            return 1;
        }
    }
    function cekRestriksiKelompok(){
        $this->load->model('Mhs_model');
        $nrp = $this->session->userdata('noid');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        if($kelType != 5 && $kelType != 6){
            //$this->err_msg = "Anda harus memiliki kelompok untuk mengakses halaman ini!";
            $GLOBALS['err_msg'] = "Anda harus memiliki kelompok untuk mengakses halaman ini!";
            redirect('Mhs/invite','refresh');
            
            //$this->invite();
            return 0;
        }
        else
            return 1;
    }
    function cekRestriksiPengajuan(){
        $this->load->model('Mhs_model');
        $this->load->model('Pengajuan_model');
        $nrp = $this->session->userdata('noid');
        $kelType = $this->Mhs_model->get_status_kelompok($nrp);
        $idKel = $this->Pengajuan_model->getIdKel($nrp, $kelType);
        $cekPengajuan = $this->Pengajuan_model->getStatusPengajuan($idKel);
        if($cekPengajuan == 0){
            
            $GLOBALS['err_msg'] = "Formulir Pengajuan Anda harus disetujui untuk mengakses halaman ini!";
            $this->home();
            return 0;
        }
        else
            return 1;
    }
    /*cek restriksi end*/
}
?>