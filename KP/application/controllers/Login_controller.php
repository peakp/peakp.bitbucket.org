<?php
	class Login_controller extends CI_Controller{
		public function __construct(){
			parent::__construct();		
		}
		//function main(){
		function index(){
			$this->load->helper(array('form'));
			$this->load->view('Login_view');
		}
		function submit(){
			$this->load->model('Login_model');		
			$this->load->driver('session');
			$username = $this->input->post('username');
			$password = sha1($this->input->post('password'));
			$check = $this->Login_model->checkID($username,$password);
			$data['check'] = $check; 
			if($check == 1){
				$this->session->set_userdata('user', 'mhs');
				$this->session->set_userdata('noid', $username);
				redirect('/Mhs/home');
			}
			else{
				$this->load->helper(array('form'));
				$this->load->view('Login_view');
			}
		}
	}
?>