<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
	}
	function view_template($current_view, $data){
		$this->load->view('header');
		$this->load->view($current_view, $data);
		return NULL;
	}
	/*login admin begin*/
	function login(){
        if($this->session->userdata('user'))
            $this->home();
        else
            $this->view_template('Login_admin_view','');
	}
	function submit(){
		$this->load->model('Login_admin_model');
		$username = $this->input->post('username');
		$password = sha1($this->input->post('password'));
		$check = $this->Login_admin_model->checkID($username,$password);
		$data['check'] = $check; 
		if($check == 1){
            $this->session->set_userdata('noid', 'admin');
			$this->session->set_userdata('user', 'admin');
			$this->view_template('Home', '');
		}
		else{
            $GLOBALS['err_msg'] = "Username / Password salah!";
			$this->login();
		}
	}
    function submitDosen(){
		$this->load->model('Dosen_model');
		$nip = $this->input->post('nip1');
		$nama = $this->input->post('nama1');
        $pass = sha1($this->input->post('pass1'));
        $telp = $this->input->post('telp1');
        $alamat = $this->input->post('alamat1');
        $status = $this->input->post('status1');
		$this->Dosen_model->updateDosen($nip,$nama,$pass,$telp,$alamat,$status);
		redirect('Admin/dm', 'refresh');
	}
    function insertDosen(){
		$this->load->model('Dosen_model');
		$nip = $this->input->post('nip1');
		$nama = $this->input->post('nama1');
        $pass = sha1($this->input->post('pass1'));
        $telp = $this->input->post('telp1');
        $alamat = $this->input->post('alamat1');
        $status = $this->input->post('status1');
        $cekNip = $this->Dosen_model->cekNip($nip);
        if($cekNip == 1){
            $GLOBALS['err_msg'] = "NIP sudah terdaftar!";
            redirect('Admin/dm', 'refresh');
            return;
        }
        else{
            $this->Dosen_model->insertDosen($nip,$nama,$pass,$telp,$alamat,$status);
            redirect('Admin/dm', 'refresh');
        }
	}
    function deleteDosen($nip){
        $this->load->model('Dosen_model');
        $this->Dosen_model->deleteDosen($nip);
        redirect('Admin/dm', 'refresh');
    }
	/*login admin end*/
	/*logout admin begin*/
    function logout(){
        $data = array(
            'noid' => '',
            'user' => '',
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('Mhs/akun','refresh');
	}
	/*logout admin end*/
		
    function akun(){
        if($this->cekRestriksiUser() == 0)
            return;
        //$GLOBALS['err_msg'] = "coba err_msg";
        $this->view_template('Akun_view', '');
    }
		
    function changePass(){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$user = $this->session->userdata('user');
		$passwordLama = $this->input->post('passwordLama');
		$passwordBaru = $this->input->post('passwordBaru');
		$sha1PasswordLama = sha1($passwordLama);
		$sha1PasswordBaru = sha1($passwordBaru);
		$this->load->model('Akun_model');
		// cek password lama
		$check = $this->Akun_model->check($user,$sha1PasswordLama);
        if($check == 1){
            $this->Akun_model->change($user,$sha1PasswordBaru);
            //$GLOBALS['err_msg'] = "Password berhasil diganti";
            //$this->akun();
            redirect('Admin/akun', 'refresh');
        }
        else{
            $GLOBALS['err_msg'] = "Password Lama salah!";
            $this->akun();
        }
    }
	/*data master begin*/
	function dm(){
        if($this->cekRestriksiUser() == 0)
            return;
        
		$this->load->model('Dosen_model');
		$data['dosenall'] = $this->Dosen_model->get_dosen_all();
		$this->view_template('Data_master', $data);
	}
	/*data master end*/
    
    function cekRestriksiUser(){
        if(!$this->session->userdata('user')){
            $GLOBALS['err_msg'] = "Anda harus Login untuk mengakses halaman ini!";
            $this->login();
            return 0;
        }
        else{
            return 1;
        }
    }
}

?>