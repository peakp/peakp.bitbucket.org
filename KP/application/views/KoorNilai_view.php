<html>
<head>
    <title>Isi Nilai Mahasiswa</title>
    <script type="text/javascript">
        $(document).ready(function () {
            (function ($) {
                $('#filter').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.searchable tr').hide();
					$('.searchable tr').filter(function () {
                        return rex.test($(this).text());
				    }).show();
				})
            }(jQuery));
		});
    </script>
</head>

<body>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
            <div class="panel panel-info">
                <div class="panel-heading">
				    <b style='font-size:150%'>
						Nilai Mahasiswa
                    </b>
				</div>
				<div class="input-group"> <span class="input-group-addon">Filter</span>
					<input id="filter" type="text" class="form-control" placeholder="Type here...">
				</div>
                <table class="table table-fixed">
                    <thead>
                        <tr>
                            <th class="col-xs-2">NRP</th>
                            <th class="col-xs-2">ID KELOMPOK</th>    
                            <th class="col-xs-2">NAMA PERUSAHAAN</th>    
                            <th class="col-xs-2">NILAI KENYATAAN</th>    
                            <th class="col-xs-2">NILAI KEDISIPLINAN</th>  
                            <th class="col-xs-2">SAVE</th>
                        </tr>
                    </thead>
                    <tbody class="searchable">
                    <?php 
                        $i = 1;
                        foreach($query as $row){
                            echo "<tr>";
                            if($i%2==1){
                                echo "<form action='submitNilai2/".$row->NRP."' method='get'>";
                                echo "<td class='col-xs-2'>". $row->NRP ."</td>";
                            }
                            else{
                                echo "<form action='submitNilai2/".$row->MHS_NRP."' method='get'>";
                                echo "<td class='col-xs-2'>". $row->MHS_NRP ."</td>";
                            }
                            echo "<td class='col-xs-2'>". $row->ID_KELOMPOK ."</td>";
                            echo "<td class='col-xs-2'>". $row->NAMA_PERUSAHAAN ."</td>";
                            echo "<td class='col-xs-2'>
                                <input type='text' name='nilaiKenyataan' value='". $row->NILAI_KENYATAAN."' class='form-control'></td>";
                            echo "<td class='col-xs-2'>
                                <input type='text' name='nilaiKedisiplinan' value='". $row->NILAI_KEDISIPLINAN."' class='form-control'></td>";
                            echo "<td class='col-xs-2'><input type='submit' name='submit' class='btn btn-default' value='Save'></td>";
                            echo "</form>";
                            echo "</tr>";   
                            $i++;
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</table>
</body>
</html>
