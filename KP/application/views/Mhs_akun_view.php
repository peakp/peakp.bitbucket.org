<html>
    <head>
        <title>Akun Mahasiswa</title>
        <script type="text/javascript">
			$(document).ready(function () {
                $('.toggle-modal').click(function(){
                    var nama = $(this).attr('data-nama');
                    var sks = $(this).attr('data-sks');
                    var telp = $(this).attr('data-telp');
                    var alamat = $(this).attr('data-alamat');
                    $('#nama').val(nama);
                    $('#sks').val(sks);
                    $('#telp').val(telp);
                    $('#alamat').val(alamat);
                    $('#myModal').modal('toggle');
                });
            });
        </script>
    </head>
    <body>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Edit Profile</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Mhs/editProfile');
                            echo form_label('Nama :', '');
                            $data = array(
                                'name' => 'nama1',
                                'id' => 'nama',
                                'class' => 'form-control',
                                'placeholder' => 'Nama');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('SKS Tempuh :', '');
                            $data = array(
                                'name' => 'sks1',
                                'id' => 'sks',
                                'class' => 'form-control',
                                'placeholder' => 'SKS Tempuh');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Alamat :', '');
                            $data = array(
                                'name' => 'alamat1',
                                'id' => 'alamat',
                                'class' => 'form-control',
                                'placeholder' => 'Alamat');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Telp :', '');
                            $data = array(
                                'name' => 'telp1',
                                'id' => 'telp',
                                'class' => 'form-control',
                                'placeholder' => 'Telp');
                            echo form_input($data);
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'SAVE');
                            echo form_submit($data);
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <b style="font-size:150%"><?php echo $mhs_data[0]->NAMA; ?></b>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12 "> 
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>NRP</b></td>
                                            <td><b>:</b> <?php echo $mhs_data[0]->NRP; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>SKS Tempuh</b></td>
                                            <td><b>:</b> <?php echo $mhs_data[0]->SKS_TEMPUH; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>Alamat</b></td>
                                            <td><b>:</b> <?php echo $mhs_data[0]->ALAMAT; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>Telp</b></td>
                                            <td><b>:</b> <?php echo $mhs_data[0]->TELP; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                                echo "<input type='button'
                                   data-nama='".$mhs_data[0]->NAMA."'
                                   data-sks='".$mhs_data[0]->SKS_TEMPUH."'
                                   data-alamat='".$mhs_data[0]->ALAMAT."'
                                   data-telp='".$mhs_data[0]->TELP."'
                                   class='toggle-modal btn btn-default col-md-4 col-lg-4'
                                   value='Edit Profile'>";
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <b style="font-size:150%">Status Kelompok</b>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12"> 
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <?php
                                                $ada_invite = 0;
                                                if($mhs_status == 6){
                                                    echo "Kelompok KP : <br/>";
                                                    echo $mhs_partner[0]->NRP." - ".$mhs_partner[0]->NAMA;
                                                }
                                                elseif($mhs_status == 5){
                                                    echo "Anda telah memilih untuk KP sendiri";
                                                }
                                                elseif($mhs_status == 4){
                                                    if($mhs_invited != NULL){
                                                        echo "Sedang menunggu konfirmasi : <br/>";
                                                        echo $mhs_invited[0]->NRP." - ".$mhs_invited[0]->NAMA;
                                                    }
                                                    else{
                                                        echo "Belum ada kelompok";
                                                        $ada_invite = 1;
                                                    }
                                                }
                                                elseif($mhs_status == 3){
                                                    echo "Seseorang telah menolak ajakan Anda!";
                                                }
                                                else
                                                    echo "Belum ada kelompok";
                                            ?>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php if($mhs_status <= 6 && $mhs_status > 4 && $mhs_pengajuan == 0){ ?>
                                    <a class="btn btn-primary" href="<?php echo site_url("Mhs/pisah_kelompok/")."/".$mhs_status; ?>">Batal</a>
                                <?php }
                                if($mhs_status == 4 && $ada_invite == 0){ ?>
                                    <a class="btn btn-primary" href="<?php echo site_url("Mhs/pisah_kelompok/")."/".$mhs_status; ?>">Batal</a>
                                <?php } ?>
                                <?php if($mhs_status < 4 || $ada_invite == 1){ ?>
                                <li class="dropdown">
                                    <a href='#' class='dropdown-toggle btn btn-primary' data-toggle='dropdown' role='button' aria-expanded='false'>
                                        Invite<span class='caret'></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo site_url('Mhs/invite'); ?>" class="">Invite / Accept Teman</a></li>
                                        <li><a href="<?php echo site_url('Mhs/kp_sendiri'); ?>" class="">KP Sendiri</a></li>
                                    </ul>
                                </li>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>