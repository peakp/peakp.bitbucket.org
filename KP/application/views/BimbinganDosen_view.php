<html>
	<head>
		<title>Bimbingan KP</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">
		<script type="text/javascript">
			$(document).ready(function () {
				(function ($) {
					$('#filter').keyup(function () {
						var rex = new RegExp($(this).val(), 'i');
						$('.searchable tr').hide();
						$('.searchable tr').filter(function () {
							return rex.test($(this).text());
						}).show();
					})
				}(jQuery));
                $('#tgl').datepicker({
                });
                $('#tgl2').datepicker({
                });
                //edit
                /*$('.toggle-modal').click(function(){
                    var idb = $(this).attr('data-id');
                    var topik = $(this).attr('data-topik');
                    var tgl = $(this).attr('data-tgl');
                    var status = $(this).attr('data-status');
                    $('#idb').val(idb);
                    $('#topik').val(topik);
                    $('#tgl').val(tgl);
                    $('#status').val(status);
                    $('#myModal').modal('toggle');
                });*/
                //insert
                /*$('.toggle-modal2').click(function(){
                    var idkp = $(this).attr('data-idkp');
                    $('#idkp').val(idkp);
                    $('#myModal2').modal('toggle');
                });*/
			});
            $(document).on("click", ".toggle-modal", function (e) {
                e.preventDefault();
                var idb = $(this).attr('data-id');
                var topik = $(this).attr('data-topik');
                var tgl = $(this).attr('data-tgl');
                var status = $(this).attr('data-status');
                $('#idb').val(idb);
                $('#topik').val(topik);
                $('#tgl').val(tgl);
                $('#status').val(status);
                //$(_self.attr('href')).modal('show');
            });
            $(document).on("click", ".toggle-modal2", function (e) {
                e.preventDefault();
                var _self = $(this);
                var idkp = _self.data('idkp');
                $("#idkp").val(idkp);
                //$(_self.attr('href')).modal('show');
            });
            $(function() {
            });
		</script>
        <style>
        </style>
	</head>
	<body>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Edit Detail Bimbingan</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Mhs/editBimbingan');
                            $data = array(
                                'name' => 'idb1',
                                'type' => 'hidden',
                                'id' => 'idb');
                            echo form_input($data);
                            echo form_label('Topik Bimbingan :', '');
                            $data = array(
                                'name' => 'topik1',
                                'id' => 'topik',
                                'class' => 'form-control',
                                'placeholder' => 'Topik Bimbingan');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Tanggal Bimbingan :', '');
                            $data = array(
                                'name' => 'tgl1',
                                'id' => 'tgl',
                                'class' => 'form-control',
                                'data-date-format' => 'yyyy-mm-dd',
                                'readonly' => "readonly",
                                'placeholder' => 'Tanggal Bimbingan');
                            echo form_input($data);
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'SAVE');
                            echo form_submit($data);
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal2" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Insert Detail Bimbingan</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Mhs/insertBimbingan');
                            $data = array(
                                'name' => 'idkp1',
                                'type' => 'hidden',
                                'id' => 'idkp');
                            echo form_input($data);
                            echo form_label('Topik Bimbingan :', '');
                            $data = array(
                                'name' => 'topik1',
                                'id' => 'topik',
                                'class' => 'form-control',
                                'placeholder' => 'Topik Bimbingan');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Tanggal Bimbingan :', '');
                            $data = array(
                                'name' => 'tgl1',
                                'id' => 'tgl2',
                                'class' => 'form-control',
                                'readonly' => "readonly",
                                'placeholder' => 'Tanggal Bimbingan');
                            echo form_input($data);
                        ?>
                            
                        <?php
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'INSERT');
                            echo form_submit($data);
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
		<?php //echo print_r ($dosenall); ?>
		<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
					<div class="panel-heading">
						<b style='font-size:150%'>Form Bimbingan</b>
					</div>
					<div class="input-group"> <span class="input-group-addon">Filter</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<table class="table table-fixed">
						<thead>
							<tr>
								<th class="col-xs-5">TOPIK BIMBINGAN</th>
								<th class="col-xs-5">TANGGAL BIMBINGAN</th>
								<th class="col-xs-2">OPSI</th>
							</tr>
						</thead>
						<tbody class="searchable">
							<?php
                            if($bimbingan_all != NULL){
								foreach($bimbingan_all as $bimb){
									echo "<tr>
										<td class='col-xs-5'>" . $bimb->TOPIK_BIMBINGAN . "</td>
										<td class='col-xs-5'>" . $bimb->TANGGAL_BIMBINGAN . "</td>
										<td class='col-xs-2'>" .
                                            "<button type='button'
                                                data-id='".$bimb->ID_DETAIL_BIMBINGAN."' 
                                                data-topik='".$bimb->TOPIK_BIMBINGAN."'
                                                data-tgl='".$bimb->TANGGAL_BIMBINGAN."'
                                                data-status='".$bimb->STATUS_DETAIL_BIMBINGAN."' 
                                                class='toggle-modal btn btn-default'
                                                data-toggle='modal' data-target='#myModal'>Edit</button>".
                                            "<a href='".site_url('Mhs/deleteBimbingan/')."/".$bimb->ID_DETAIL_BIMBINGAN."'>
                                                <input type='button' class='btn btn-default' value='Delete' />
                                            </a>".
                                        "</td>
								    </tr>";
								}
                            }
							?>
						</tbody>
					</table>
                    <?php
                        //echo "<input type='button' data-idkp='".$idkp."' class='toggle-modal2 btn btn-default' value='Insert'> ";
                        echo "<button type='button' class='btn btn-default toggle-modal2'
                            data-toggle='modal' data-target='#myModal2' data-idkp='".$idkp."'>Insert</button>";
                        echo "<a href='".site_url('Mhs/pdfBimbing')."/".$idkp."' class='btn btn-primary' target='_blank'>Print</a>";
                    ?>
				</div>
			</div>
		</div>
	</body>
</html>