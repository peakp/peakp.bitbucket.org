<html>
    <?php 
        $filetitle = '';
        if($filetp == 'surat_persetujuan_kp')
            $filetitle = "Surat Persetujuan KP";
        elseif($filetp == 'surat_pengumpulan_buku')
            $filetitle = "Surat Pengumpulan Buku";
    ?>
    <head>
        <title>Upload <?php echo $filetitle ?></title>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#img-modal').click(function(){
                    $('#imagepreview').attr('src', $('#imageresource').attr('src'));
                    $('#myModal').modal('show');
                });
            });
        </script>
    </head>
    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="">
                        <b style="font-size:150%">Form Upload</b>
                    </div>
                    <div class="panel-body" style="">
        <?php

        echo "<b>FILE ". $filetitle ." ANDA :</b><br>";
        if($current_file[0]->IMAGE_FILE != ''){
            //echo "<img src='".$current_file[0]->IMAGE_FILE."'>";
            echo "
            <a href='#' id='img-modal'>
                <img href='#' id='imageresource' src='".base_url($current_file[0]->IMAGE_FILE)."' style='max-height:256px; margin:2.5%'/>
            </a>";
        }
        else
            echo "NO IMAGE UPLOADED";
        ?>

        <?php echo form_open_multipart('upload/do_upload');

        $data = array(
            'name' => 'userfile',
            'type' => 'file',
            'size' => '20'
        );
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'name' => 'filetype',
            'type' => 'hidden',
            'id' => 'filetype',
            'value' => $filetp);
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'value' => 'Upload'
        );
        echo form_submit($data);
        echo form_close();
        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <img src="" id="imagepreview" style="" >
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>