<html>
    <head>
        <title>Login Admin</title>
    </head>
    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="text-align:center">
                        <b style="font-size:150%; text-align:center;">LOGIN ADMIN</b>
                    </div>
                    <div class="panel-body" style="text-align:left;">
					<?php
						echo form_open('Admin/submit');
						echo form_label('Username :', 'usernames');
						$data = array(
				            'name' => 'username',
                            'class' => 'form-control',
				            'placeholder' => 'Username');
						echo form_input($data);
                        echo "<br>";
						echo form_label('Password :', 'passwords');
						$data = array(
                            'name' => 'password',
                            'class' => 'form-control',
				            'placeholder' => 'Password');
						echo form_password($data);   
                        echo "<br>";
						$data = array(
				            'type' => 'submit',
                            'class' => 'btn btn-primary',
				            'value' => 'LOGIN');
						echo form_submit($data);   
						echo form_close()
					?>
				    </div>
                </div>
            </div>
        </div>
    </body>
</html>                                		