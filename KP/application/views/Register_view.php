<html>
    <head>
        <title>Registrasi Mahasiswa</title>
    </head>
    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="text-align:center">
                        <b style="font-size:150%; text-align:center;">REGISTRASI MAHASISWA</b>
                    </div>
                    <div class="panel-body" style="text-align:left;">
                        <?php
                        echo form_open('Mhs/submitRegister');

                        echo form_label('NRP :', 'usernames');
                        $data = array(
                            'name' => 'nrp',
                            'class' => 'form-control',
                            'placeholder' => 'NRP');
                        echo form_input($data);
                        echo "<br/>";
                        echo form_label('Nama :', 'nama');
                        $data = array(
                            'class' => 'form-control',
                            'name' => 'nama',
                            'placeholder' => 'Nama');
                        echo form_input($data);
                        echo "<br/>";
                        echo form_label('Password :', 'passwords');
                        $data = array(
                            'name' => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'Password');
                        echo form_password($data);
                        echo "<br/>";
                        echo form_label('Confirm Password :', 'confirm');
                        $data = array(
                            'name' => 'confirm',
                            'class' => 'form-control',
                            'placeholder' => 'Confirm Password');
                        echo form_password($data); 
                        echo "<br/>";
                        echo form_label('Sks yang telah ditempuh :', 'sks');
                        $data = array(
                            'name' => 'sks',
                            'class' => 'form-control',
                            'placeholder' => 'SKS Tempuh');
                        echo form_input($data);
                        echo "<br/>";
                        echo form_label('Alamat :', 'alamat');
                        $data = array(
                            'name' => 'alamat',
                            'class' => 'form-control',
                            'placeholder' => 'Alamat');
                        echo form_input($data);
                        echo "<br/>";
                        echo form_label('Telepon :', 'telepon');
                        $data = array(
                            'name' => 'telepon',
                            'class' => 'form-control',
                            'placeholder' => 'Telepon');
                        echo form_input($data);
                        echo "<br/>";
                        $data = array(
                         'type' => 'submit',
                            'class' => 'btn btn-primary',
                         'value' => 'SUBMIT');
                        echo form_submit($data);   
                        echo form_close()
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>