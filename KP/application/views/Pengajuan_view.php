<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
	<title>Pengajuan KP</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
		$(function() {
			$( "#datepicker" ).datepicker({
				dateFormat :"yy-mm-dd"
			});
			$( "#datepicker2" ).datepicker({
				dateFormat :"yy-mm-dd"
			});
		});
    </script>
 </head>
 <body>
     <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
					<div class="panel-heading">
						<b style='font-size:150%'>Form Pengajuan KP</b>
					</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-6 col-lg-6 "> 
	<?php
	$attributes = array('target' => '_blank');
	echo form_open('Mhs/submitPengajuan');
	echo form_label('Nama Perusahaan :', 'namaPerusahaan');
	$data = array(
	 'name' => 'namaPerusahaan',
        'class' => 'form-control',
	 'value' => $namaPerusahaan);
	echo form_input($data);
	echo "<br/>";
	echo form_label('Alamat Perusahaan :', 'alamatPerusahaan');
	$data = array(
	 'name' => 'alamatPerusahaan',
        'class' => 'form-control',
	 'value' => $alamatPerusahaan);
	echo form_input($data);   
	echo "<br/>";
	echo form_label('Telepon Perusahaan :', 'telpPerusahaan');
	$data = array(
	 'name' => 'telpPerusahaan',
        'class' => 'form-control',
	 'value' => $telpPerusahaan);
	echo form_input($data);   
	echo "<br/>";
	echo form_label('Nama Wakil :', 'namaWakil');
	$data = array(
	 'name' => 'namaWakil',
        'class' => 'form-control',
	 'value' => $namaWakil);
	echo form_input($data);
	echo "<br/>";
	echo form_label('Tanggal Mulai :', 'tanggalMulai');
	$data = array(
	 'name' => 'tanggalMulai',
        'class' => 'form-control',
	 'value' => $tanggalMulai,
	 'id' => 'datepicker');
	echo form_input($data);   
	echo "<br/>";
	echo form_label('Tanggal Selesai :', 'tanggalSelesai');
	$data = array(
	 'name' => 'tanggalSelesai',
        'class' => 'form-control',
	 'value' => $tanggalSelesai,
	 'id' => 'datepicker2');
	echo form_input($data);
	echo "<br/>";
	$data = array(
        'name' => 'save',
        'type' => 'submit',
        'class' => 'btn btn-default',
        'value' => 'SAVE');
	echo form_submit($data); 
    $data = array(
        'formtarget' => '_blank',
        'name' => 'print',
        'type' => 'submit',
        'class' => 'btn btn-primary',
        'value' => 'PRINT');
	echo form_submit($data); 
	echo form_close();
	?>
                            </div>
                        </div>
                    </div>
                </div>
         </div>
     </div>
 </body>
</html>