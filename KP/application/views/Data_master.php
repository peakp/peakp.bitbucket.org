<html>
	<head>
		<title>Data Master</title>
		<script type="text/javascript">
			$(document).ready(function () {
				(function ($) {
					$('#filter').keyup(function () {
						var rex = new RegExp($(this).val(), 'i');
						$('.searchable tr').hide();
						$('.searchable tr').filter(function () {
							return rex.test($(this).text());
						}).show();
					})
				}(jQuery));
                
                $('.toggle-modal').click(function(){
                    var nip = $(this).attr('data-nip');
                    var nama = $(this).attr('data-nama');
                    var pass = $(this).attr('data-pass');
                    var telp = $(this).attr('data-telp');
                    var alamat = $(this).attr('data-alamat');
                    var status = $(this).attr('data-status');
                    $('#nip').val(nip);
                    $('#nama').val(nama);
                    $('#pass').val(pass);
                    $('#telp').val(telp);
                    $('#alamat').val(alamat);
                    $('#status').val(status);
                    $('#myModal').modal('toggle');
                });
                $('.toggle-modal2').click(function(){
                    var nip = $(this).attr('data-nip');
                    var nama = $(this).attr('data-nama');
                    var pass = $(this).attr('data-pass');
                    var telp = $(this).attr('data-telp');
                    var alamat = $(this).attr('data-alamat');
                    var status = $(this).attr('data-status');
                    $('#nip').val(nip);
                    $('#nama').val(nama);
                    $('#pass').val(pass);
                    $('#telp').val(telp);
                    $('#alamat').val(alamat);
                    $('#status').val(status);
                    $('#myModal2').modal('toggle');
                });
			});
		</script>
	</head>
	<body>
		<?php //echo print_r ($dosenall); ?>
		<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
					<div class="panel-heading">
						<b style='font-size:150%'>
							Data Master Dosen
                        </b>
					</div>
					<div class="input-group"> <span class="input-group-addon">Filter</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<table class="table table-fixed">
						<thead>
							<tr>
								<th class="col-xs-2">NIP</th>
								<th class="col-xs-2">Nama</th>
                                <th class="col-xs-2">Telp</th>
                                <th class="col-xs-2">Alamat</th>
								<th class="col-xs-2">Status</th>
								<th class="col-xs-2">Opsi</th>
							</tr>
						</thead>
						<tbody class="searchable">
							<?php
								foreach($dosenall as $dosenrow){
									echo "<tr>
										<td class='col-xs-2'>" . $dosenrow->NIP . "</td>
										<td class='col-xs-2'>" . $dosenrow->NAMA_DOSEN . "</td>
										<td class='col-xs-2'>" . $dosenrow->TELP_DOSEN . "</td>
                                        <td class='col-xs-2'>" . $dosenrow->ALAMAT_DOSEN . "</td>
                                        <td class='col-xs-2'>" . $dosenrow->STATUS_DOSEN . "</td>
										<td class='col-xs-2'>" .
                                            "<input type='button'
                                                data-nip='".$dosenrow->NIP."' 
                                                data-nama='".$dosenrow->NAMA_DOSEN."'
                                                data-pass='".$dosenrow->PASSWORD_DOSEN."' 
                                                data-telp='".$dosenrow->TELP_DOSEN."' 
                                                data-alamat='".$dosenrow->ALAMAT_DOSEN."' 
                                                data-status='".$dosenrow->STATUS_DOSEN."' 
                                            class='toggle-modal btn btn-default' value='Edit'>".
                                            "<a href='".site_url('Admin/deleteDosen/')."/".$dosenrow->NIP."'>
                                                <input type='button' class='btn btn-default' value='Delete' />
                                            </a>".
                                        "</td>
								    </tr>";
								}
							?>
						</tbody>
					</table>
                    <?php
                        echo "<input type='button' class='toggle-modal2 btn btn-default' value='Insert'>";
                    ?>
				</div>
			</div>
		</div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Edit Data Dosen</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Admin/submitDosen');
                            $data = array(
                                'name' => 'nip1',
                                'type' => 'hidden',
                                'id' => 'nip');
                            echo form_input($data);
                            echo form_label('Nama :', '');
                            $data = array(
                                'name' => 'nama1',
                                'id' => 'nama',
                                'class' => 'form-control',
                                'placeholder' => 'Nama');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Password :', '');
                            $data = array(
                                'name' => 'pass1',
                                'id' => 'pass',
                                'class' => 'form-control',
                                'placeholder' => 'Password');
                            echo form_password($data);
                            echo "<br>";
                            echo form_label('Telp :', '');
                            $data = array(
                                'name' => 'telp1',
                                'id' => 'telp',
                                'class' => 'form-control',
                                'placeholder' => 'Telp');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Alamat :', '');
                            $data = array(
                                'name' => 'alamat1',
                                'id' => 'alamat',
                                'class' => 'form-control',
                                'placeholder' => 'Alamat');
                            echo form_input($data);
                            echo "<br>";
                            $options = array(
                                'Dosbing' => 'Dosen Pembimbing',
                                'Koor' => 'Koordinator'
                            );
                            echo form_label('Status : ', '');
                            echo form_dropdown('status1', $options, '',"id='status'");
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'SAVE');
                            echo form_submit($data);
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal2" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Insert Data Dosen</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Admin/insertDosen');
                            $data = array(
                                'name' => 'nip1',
                                'id' => 'nip',
                                'maxlength' => "20",
                                'class' => 'form-control',
                                'placeholder' => 'NIP');
                            echo form_input($data);
                            echo form_label('Nama :', '');
                            $data = array(
                                'name' => 'nama1',
                                'id' => 'nama',
                                'class' => 'form-control',
                                'placeholder' => 'Nama');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Password :', '');
                            $data = array(
                                'name' => 'pass1',
                                'id' => 'pass',
                                'class' => 'form-control',
                                'placeholder' => 'Password');
                            echo form_password($data);
                            echo "<br>";
                            echo form_label('Telp :', '');
                            $data = array(
                                'name' => 'telp1',
                                'id' => 'telp',
                                'class' => 'form-control',
                                'placeholder' => 'Telp');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Alamat :', '');
                            $data = array(
                                'name' => 'alamat1',
                                'id' => 'alamat',
                                'class' => 'form-control',
                                'placeholder' => 'Alamat');
                            echo form_input($data);
                            echo "<br>";
                            $options = array(
                                'Dosbing' => 'Dosen Pembimbing',
                                'Koor' => 'Koordinator'
                            );
                            echo form_label('Status : ', '');
                            echo form_dropdown('status1', $options, '',"id='status'");
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'INSERT');
                            echo form_submit($data);   
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>