<html>
    <head>
        <title>Persetujuan KP</title>
        <script>
            $(document).ready(function () {
                (function ($) {
                    $('#filter').keyup(function () {
                        var rex = new RegExp($(this).val(), 'i');
                        $('.searchable tr').hide();
                        $('.searchable tr').filter(function () {
                            return rex.test($(this).text());
                        }).show();
                    })
                }(jQuery));

                $('.img-modal').click(function(){
                    //var src = $(this).find('img').attr('src');
                    $('#imagepreview').attr('src', $(this).find('img').attr('src'));
                    $('#myModal').modal('show');
                });
            });
        </script>
    </head>

    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
				        <b style='font-size:150%'>
                            Tabel Persetujuan KP Mahasiswa
                        </b>
				    </div>
				    <div class="input-group"> <span class="input-group-addon">Filter</span>
					   <input id="filter" type="text" class="form-control" placeholder="Type here...">
				    </div>
                    <table class="table table-fixed">
                        <thead>
                            <tr>
                                <th class="col-xs-2">ID KELOMPOK</th>    
                                <th class="col-xs-2">NAMA PERUSAHAAN</th>
                                <th class="col-xs-2">SURAT PERSETUJUAN</th>
                                <th class="col-xs-2">DOSEN PEMBIMBING</th>
                                <th class="col-xs-2">SETUJU</th>    
                                <th class="col-xs-2">TOLAK</th>  
                            </tr>
                        </thead>
                        <tbody class="searchable">
                        <?php 
                            foreach($query as $row){
                                echo "<tr>";
                                echo "<td class='col-xs-2'>". $row->ID_KELOMPOK ."</td>";
                                echo "<td class='col-xs-2'>". $row->NAMA_PERUSAHAAN ."</td>";
                                if($row->SURAT_PERSETUJUAN_KP == NULL){
                                    echo "<td class='col-xs-2'>No image uploaded</td>";
                                }
                                else{
                                    echo "<td class='col-xs-2'><a href='#' class='img-modal'>
                                            <img href='#'
                                            src='".base_url($row->SURAT_PERSETUJUAN_KP)."'
                                            style='max-height:32px;'/>
                                        </a>
                                        </td>";
                                }
                                echo form_open('Dosen/setuju/'.$row->ID_KP);
                                echo "<td class='col-xs-2'>". form_dropdown('dosbing_id', $dosbing,'', 'id="dosbing_id"') ."</td>";
                                $data = array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-default',
                                    'value' => 'Setuju'
                                );
                                echo "<td class='col-xs-2'>".form_submit($data)."</td>"; 
                                echo form_close();
                                echo form_open('Dosen/tolak/'.$row->ID_KP);
                                $data = array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-default',
                                    'value' => 'Tolak'
                                );
                                echo "<td class='col-xs-2'>".form_submit($data)."</td>"; 
                                echo form_close();
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <img src="" id="imagepreview" style="" >
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
