<html>
    <head>
        <title>Dokumen Mahasiswa</title>
        <script>
            $(document).ready(function () {
                (function ($) {
                    $('#filter').keyup(function () {
                        var rex = new RegExp($(this).val(), 'i');
                        $('.searchable tr').hide();
                        $('.searchable tr').filter(function () {
                            return rex.test($(this).text());
                        }).show();
                    })
                }(jQuery));

                $('.img-modal').click(function(){
                    //var src = $(this).find('img').attr('src');
                    $('#imagepreview').attr('src', $(this).find('img').attr('src'));
                    $('#myModal').modal('show');
                });
                
                $('.toggle-modal').click(function(){
                    var idkel = $(this).attr('data-idkel');
                    var nrp1 = $(this).attr('data-nrp1');
                    var nrp2 = $(this).attr('data-nrp2');
                    var nama1 = $(this).attr('data-nama1');
                    var nama2 = $(this).attr('data-nama2');
                    $('#idkel').val(idkel);
                    $('#nrp1').val(nrp1);
                    $('#nrp2').val(nrp2);
                    $('#nama1').val(nama1);
                    $('#nama2').val(nama2);
                    $('#myModal2').modal('toggle');
                });
            });
        </script>
    </head>

    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
				        <b style='font-size:150%'>
				            Tabel Dokumen Mahasiswa
                        </b>
				    </div>
				    <div class="input-group"> <span class="input-group-addon">Filter</span>
					   <input id="filter" type="text" class="form-control" placeholder="Type here...">
				    </div>
                    <table class="table table-fixed">
                        <thead>
                                <tr>
                                    <th class="col-xs-1">DETAIL KELOMPOK</th>
                                    <th class="col-xs-2">NAMA PERUSAHAAN</th>
                                    <th class="col-xs-2">SURAT PERSETUJUAN</th>
                                    <th class="col-xs-2">SURAT PENGUMPULAN BUKU</th>
                                    <th class="col-xs-2">LEMBAR PENILAIAN 1</th>
                                    <th class="col-xs-2">LEMBAR PENILAIAN 2</th>
                                    <th class="col-xs-1">OPSI</th>
                                </tr>
                            </thead>
                        <tbody class="searchable">
                        <?php 

                            foreach($kel_all as $row){
                                echo "<tr>";
                                echo "<td class='col-xs-1'><a href='#' class='toggle-modal'
                                    data-idkel='".$row->ID_KELOMPOK."'
                                    data-nrp1='".$row->NRP."'
                                    data-nrp2='".$row->MHS_NRP."'
                                    data-nama1='".$row->NAMA1."'
                                    data-nama2='".$row->NAMA2."'>"
                                        .$row->ID_KELOMPOK.
                                    "</a></td>";
                                echo "<td class='col-xs-2'>". $row->NAMA_PERUSAHAAN ."</td>";
                                if($row->SURAT_PERSETUJUAN_KP == NULL){
                                    echo "<td class='col-xs-2'>No image uploaded</td>";
                                }
                                else{
                                    echo "<td class='col-xs-2'><a href='#' class='img-modal'><img href='#'
                                            src='".base_url($row->SURAT_PERSETUJUAN_KP)."' style='max-height:32px;'/>
                                        </a></td>";
                                }
                                if($row->SURAT_PENGUMPULAN_BUKU == NULL){
                                    echo "<td class='col-xs-2'>No image uploaded</td>";
                                }
                                else{
                                    echo "<td class='col-xs-2'><a href='#' class='img-modal'><img href='#'
                                            src='".base_url($row->SURAT_PENGUMPULAN_BUKU)."' style='max-height:32px;'/>
                                        </a></td>";
                                }
                                if($row->LEMBAR_NILAI_PERUSAHAAN1 == NULL){
                                    echo "<td class='col-xs-2'>No image uploaded</td>";
                                }
                                else{
                                    echo "<td class='col-xs-2'><a href='#' class='img-modal'><img href='#'
                                            src='".base_url($row->LEMBAR_NILAI_PERUSAHAAN1)."' style='max-height:32px;'/>
                                        </a></td>";
                                }
                                if($row->LEMBAR_NILAI_PERUSAHAAN2 == NULL){
                                    echo "<td class='col-xs-2'>No image uploaded</td>";
                                }
                                else{
                                    echo "<td class='col-xs-2'><a href='#' class='img-modal'><img href='#'
                                            src='".base_url($row->LEMBAR_NILAI_PERUSAHAAN2)."' style='max-height:32px;'/>
                                        </a></td>";
                                }
                                echo "<td class='col-xs-1'>Reserved Opsi</td>";
                                echo "</tr>";
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <img src="" id="imagepreview" style="" >
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal2" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Detail Kelompok</h4>
                    </div>
                    <div class="modal-body">
                        <label>Mahasiswa 1</label><br/>
                        NRP : <input type="text" class='' id='nrp1' disabled>
                        NAMA : <input type="text" class='' id='nama1' disabled><br/><br/>
                        <label>Mahasiswa 2</label><br/>
                        NRP : <input type="text" class='' id='nrp2' disabled>
                        NAMA : <input type="text" class='' id='nama2' disabled>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
