<?php
//$GLOBALS['err_msg'] must ""
//redirect not showing $GLOBALS['err_msg']
//function, view_template showing $GLOBALS['err_msg']
if($GLOBALS['err_msg'] != ""){
    echo '<script language="javascript">';
    echo 'alert("'.$GLOBALS['err_msg'].'")';
    echo '</script>';
    $GLOBALS['err_msg'] = "";
}
else{
    $GLOBALS['err_msg'] = "";
}
/*print_r ($err_msg);
if($err_msg != ""){
    echo '<script language="javascript">';
    echo 'alert("'.err_msg.'")';
    echo '</script>';
    $err_msg = "";
}
else{
    $err_msg = "";
}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url('/assets/style5.css'); ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
    </script>
</head>
<body class="container">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo site_url('Mhs/home'); ?>">Home<span class="sr-only">(current)</span></a></li>
					<?php if($this->session->userdata('user') == 'admin'){
                        echo "<li><a href=" . site_url("Admin/akun") . ">Ganti Password</a></li>";
                        echo "<li><a href=" . site_url("Admin/dm") . ">Data Master</a></li>";
                        echo '<li class="dropdown">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Opsi Koordinator<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="'.site_url("Dosen/persetujuan").'">Persetujuan KP</a></li>
                                <li><a href="'.site_url("Dosen/KoorNilai").'">Isi Nilai Mahasiswa</a></li>
                                <li><a href="'.site_url("Dosen/rekap").'">Rekap Nilai Mahasiswa</a></li>
                                <li><a href="'.site_url("Dosen/rekapDokumen").'">Rekap Dokumen Mahasiswa</a></li>
                            </ul>
                        </li>';
                        echo '<li class="dropdown">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Opsi Dosbing<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="'.site_url("Dosen/mainNilai2").'">Isi Nilai Mahasiswa</a></li>
                            </ul>
                        </li>';
                    } ?>
					<?php if($this->session->userdata('user') == 'mhs'){
						echo "<li><a href=" . site_url("Mhs/akun") . ">Akun / Kelompok</a></li>";
                        echo "<li><a href=" . site_url("Mhs/pengajuan") . ">Pengajuan</a></li>";
                        echo "<li><a href=" . site_url("Mhs/bimbingan") . ">Bimbingan</a></li>";
                        echo '<li class="dropdown">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Upload<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="'.site_url('Upload/viewForm1/surat_persetujuan_kp').'">Surat Persetujuan KP</a></li>
                                <li><a href="'.site_url('Upload/viewForm1/surat_pengumpulan_buku').'">Surat Pengumpulan Buku KP</a></li>
                                <li><a href="'.site_url('Upload/viewForm2').'">Lembar Penilaian Perusahaan</a></li>
                            </ul>
                        </li>
                        ';
					} ?>
					<?php if($this->session->userdata('user') == 'koor'){
						echo "<li><a href=" . site_url("Dosen/akun") . ">Akun</a></li>";
						echo "<li><a href=" . site_url("Dosen/persetujuan") . ">Persetujuan KP</a></li>";
                        echo "<li><a href=" . site_url("Dosen/KoorNilai") . ">Isi Nilai Mahasiswa</a></li>";
                        echo "<li><a href=" . site_url("Dosen/rekap") . ">Rekap Nilai Mahasiswa</a></li>";
                        echo "<li><a href=" . site_url("Dosen/rekapDokumen") . ">Rekap Dokumen Mahasiswa</a></li>";
					} ?>
					<?php if($this->session->userdata('user') == 'dosbing'){
						echo "<li><a href=" . site_url("Dosen/akun") . ">Akun</a></li>";
						echo "<li><a href=" . site_url("Dosen/mainNilai") . ">Isi Nilai Mahasiswa</a></li>";
                        echo "<li><a href=" . site_url("Dosen/rekap") . ">Rekap Nilai Mahasiswa</a></li>";
					} ?>
                    <?php ?>
				</ul>
				<ul class="nav navbar-nav navbar-right">
                    <?php if($this->session->userdata('user')){}else{ ?>
                    <li><a href="<?php echo site_url('Mhs/register') ?>">Registrasi</a></li>
                    <?php } ?>
                    <li class="dropdown">
                        <?php if($this->session->userdata('user')){?>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>
                                <?php echo $this->session->userdata('noid')?><span class='caret'></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                    <?php echo "<li><a href='".site_url('Mhs/logout')."'>Logout</a></li>"; ?>
                            </ul>
                        <?php
                        }
                        else {?>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>
                                Login<span class='caret'></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                    echo "<li><a href='".site_url("Mhs/login")."'>Mhs</a></li>";
                                    echo "<li><a href='".site_url("Dosen/main")."'>Dosen</a></li>";
                                ?>
                            </ul>
                        <?php

                        }?>
                    </li>
				</ul>
			</div>
		</div>
	</nav>
</body>
</html>                                		