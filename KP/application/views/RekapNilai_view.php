<html>
    <head>
        <title>Rekap Nilai</title>
        <script type="text/javascript">
			$(document).ready(function () {
				(function ($) {
					$('#filter').keyup(function () {
						var rex = new RegExp($(this).val(), 'i');
						$('.searchable tr').hide();
						$('.searchable tr').filter(function () {
							return rex.test($(this).text());
						}).show();
					})
				}(jQuery));
			});
		</script>
		<style type="text/css">
			
		</style>
    </head>
    <body>
        <div class="container">
			<div class="row">
				<div class="panel panel-info">
					<div class="panel-heading">
                        <b style="font-size:150%">Rekap Nilai Mahasiswa</b>
					</div>
					<div class="input-group"> <span class="input-group-addon">Filter</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<table class="table table-fixed" style="">
						<thead>
							<tr>
								<th class='col-xs-1'>NRP</th>
								<th class='col-xs-2'>Nama</th>
								<th class='col-xs-2'>Nilai Lisan</th>
                                <th class='col-xs-2'>Nilai Buku</th>
                                <th class='col-xs-2'>Nilai Kenyataan</th>
                                <th class='col-xs-2'>Nilai Kedisiplinan</th>
                                <th class='col-xs-1'>Nilai Akhir</th>
							</tr>
						</thead>
						<tbody class="searchable">
							<?php
								foreach($mhs_all as $mhsrow){
                                    $n_akhir=($mhsrow->NILAI_LISAN+$mhsrow->NILAI_BUKU+$mhsrow->NILAI_KENYATAAN+$mhsrow->NILAI_KEDISIPLINAN)/4;
									echo "<tr>
										<td class='col-xs-1'>" . $mhsrow->NRP . "</td>
										<td class='col-xs-2'>" . $mhsrow->NAMA . "</td>
										<td class='col-xs-2'>" . $mhsrow->NILAI_LISAN . "</td>
                                        <td class='col-xs-2'>" . $mhsrow->NILAI_BUKU . "</td>
                                        <td class='col-xs-2'>" . $mhsrow->NILAI_KENYATAAN . "</td>
                                        <td class='col-xs-2'>" . $mhsrow->NILAI_KEDISIPLINAN . "</td>
                                        <td class='col-xs-1'>" . number_format($n_akhir, 2, '.', ',') . "</td>
									</tr>";
								}
							?>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </body>
</html>