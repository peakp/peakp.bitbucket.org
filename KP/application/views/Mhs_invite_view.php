<html>
	<head>
		<title>Invite Teman</title>
		<script type="text/javascript">
			$(document).ready(function () {
				(function ($) {
					$('#filter').keyup(function () {
						var rex = new RegExp($(this).val(), 'i');
						$('.searchable tr').hide();
						$('.searchable tr').filter(function () {
							return rex.test($(this).text());
						}).show();
					})
				}(jQuery));
			});
		</script>
	</head>
	<body>
		<?php //echo print_r ($dosenall); ?>
		<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <b style="font-size:100%">Mahasiswa yang belum berkelompok :</b>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12 "> 
                                <table class="table table-user-information">
                                    <thead>
                                        <tr>
                                            <th class="col-xs-4">NRP</th>
                                            <th class="col-xs-4">Nama</th>
                                            <th class="col-xs-4">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="searchable">
                                        <?php
                                            foreach($mhs_available as $mhsrow){
                                                $cross = 0;
                                                foreach($mhs_inviting as $mhscek){
                                                    if($mhsrow->NRP == $mhscek->NRP)
                                                        $cross=1;
                                                }
                                                if($mhsrow->NRP != $this->session->userdata('noid') && $cross != 1){
                                                echo "<tr>
                                                    <td class='col-xs-4'>" . $mhsrow->NRP . "</td>
                                                    <td class='col-xs-4'>" . $mhsrow->NAMA . "</td>
                                                    <td class='col-xs-4'><a href='" . site_url("Mhs/invite_teman") . "/" . $mhsrow->NRP ."'>Invite</a></td>
                                                    </tr>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <b style="font-size:100%">Mahasiswa yang menginvite Anda : <?php echo count($mhs_inviting); ?></b>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12 "> 
                                <table class="table table-user-information">
                                    <thead>
                                        <tr>
                                            <th class="col-xs-4">NRP</th>
                                            <th class="col-xs-4">Nama</th>
                                            <th class="col-xs-4">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="searchable">
                                        <?php
                                            foreach($mhs_inviting as $mhsinv){
                                                echo "<tr>
                                                    <td class='col-xs-4'>" . $mhsinv->NRP . "</td>
                                                    <td class='col-xs-4'>" . $mhsinv->NAMA . "</td>
                                                    <td class='col-xs-4'>
                                                        <a href='".site_url("Mhs/accept_teman")."/".$mhsinv->NRP."'>Terima</a> / 
                                                        <a href='".site_url("Mhs/tolak_teman")."/".$mhsinv->NRP."'>Tolak</a>
                                                    </td>
                                                    </tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</body>
</html>