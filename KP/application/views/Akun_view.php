<html>
    <head>
        <title>Ganti Password</title>
    </head>
    <body>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="text-align:center">
                        <b style="font-size:150%">GANTI PASSWORD ADMIN</b>
                    </div>
                    <div class="panel-body" style="">
                    <?php
                        echo form_open('Admin/changePass');
                        echo form_label('Password lama:', 'passwords');
                        $data = array(
                            'class' => 'form-control',
                            'name' => 'passwordLama');
                        echo form_password($data);
                        echo "<br>";
                        echo form_label('Password baru:', 'passwords');
                        $data = array(
                            'class' => 'form-control',
                            'name' => 'passwordBaru');
                        echo form_password($data);
                        echo "<br>";
                        $data = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary',
                            'value' => 'SAVE');
                        echo form_submit($data);   
                        echo form_close()
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>