<html>
    <head>
        <title>Akun Dosen</title>
        <script type="text/javascript">
			$(document).ready(function () {
                $('.toggle-modal').click(function(){
                    var nama = $(this).attr('data-nama');
                    var telp = $(this).attr('data-telp');
                    var alamat = $(this).attr('data-alamat');
                    $('#nama').val(nama);
                    $('#telp').val(telp);
                    $('#alamat').val(alamat);
                    $('#myModal').modal('toggle');
                });
            });
        </script>
    </head>
    <body>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Edit Profile</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            echo form_open('Dosen/editProfile');
                            echo form_label('Nama :', '');
                            $data = array(
                                'name' => 'nama1',
                                'id' => 'nama',
                                'class' => 'form-control',
                                'placeholder' => 'Nama');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Alamat :', '');
                            $data = array(
                                'name' => 'alamat1',
                                'id' => 'alamat',
                                'class' => 'form-control',
                                'placeholder' => 'Alamat');
                            echo form_input($data);
                            echo "<br>";
                            echo form_label('Telp :', '');
                            $data = array(
                                'name' => 'telp1',
                                'id' => 'telp',
                                'class' => 'form-control',
                                'placeholder' => 'Telp');
                            echo form_input($data);
                            echo "<br>";
                            $data = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'SAVE');
                            echo form_submit($data);
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><b style="font-size:150%"><?php echo $dosen_data[0]->NAMA_DOSEN; ?></b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12 "> 
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>NIP</b></td>
                                            <td><b>:</b> <?php echo $dosen_data[0]->NIP; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>Alamat</b></td>
                                            <td><b>:</b> <?php echo $dosen_data[0]->ALAMAT_DOSEN; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>Telp</b></td>
                                            <td><b>:</b> <?php echo $dosen_data[0]->TELP_DOSEN; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 col-lg-4"><b>Status Dosen</b></td>
                                            <td><b>:</b> <?php
                                                if($dosen_data[0]->STATUS_DOSEN == 'Dosbing')
                                                    echo "Dosen Pembimbing";
                                                elseif($dosen_data[0]->STATUS_DOSEN == 'Koor')
                                                    echo "Koordinator KP";?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                                echo "<input type='button'
                                   data-nama='".$dosen_data[0]->NAMA_DOSEN."'
                                   data-alamat='".$dosen_data[0]->ALAMAT_DOSEN."'
                                   data-telp='".$dosen_data[0]->TELP_DOSEN."'
                                   class='toggle-modal btn btn-default col-md-4 col-lg-4'
                                   value='Edit Profile'>";
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>