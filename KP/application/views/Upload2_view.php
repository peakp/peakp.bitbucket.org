<html>
    <head>
        <title>Upload Lembar Penilaian Perusahaan</title>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.img-modal').click(function(){
                    //var src = $(this).find('img').attr('src');
                    $('#imagepreview').attr('src', $(this).find('img').attr('src'));
                    $('#myModal').modal('show');
                });
            });
        </script>
        <style>
            .modal-body {
                position: relative;
                overflow-y:auto;
                height:auto;
                width:auto;
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="">
                        <b style="font-size:150%">Form Upload Lembar Penilaian 1</b>
                    </div>
                    <div class="panel-body" style="">
        <?php

        echo "<b>FILE PENILAIAN 1 ANDA :</b><br>";
        if($current_file[0]->IMAGE_FILE_1 != ''){
            //echo "<img src='".$current_file[0]->IMAGE_FILE."'>";
            echo "
            <a href='#' class='img-modal'>
                <img href='#' src='".base_url($current_file[0]->IMAGE_FILE_1)."' style='max-height:256px; margin:2.5%'/>
            </a>";
        }
        else
            echo "NO IMAGE UPLOADED";
        ?>

        <?php echo form_open_multipart('upload/do_upload');

        $data = array(
            'name' => 'userfile',
            'type' => 'file',
            'size' => '20'
        );
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'name' => 'filetype',
            'type' => 'hidden',
            'id' => 'filetype',
            'value' => 'lembar_nilai_perusahaan1');
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'value' => 'Upload'
        );
        echo form_submit($data);
        echo form_close();
        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading" style="">
                        <b style="font-size:150%">Form Upload Lembar Penilaian 2</b>
                    </div>
                    <div class="panel-body" style="">
        <?php

        echo "<b>FILE PENILAIAN 2 ANDA :</b><br>";
        if($current_file[0]->IMAGE_FILE_2 != ''){
            //echo "<img src='".$current_file[0]->IMAGE_FILE."'>";
            echo "
            <a href='#' class='img-modal'>
                <img href='#' src='".base_url($current_file[0]->IMAGE_FILE_2)."' style='max-height:256px; margin:2.5%'/>
            </a>";
        }
        else
            echo "NO IMAGE UPLOADED";
        ?>

        <?php echo form_open_multipart('upload/do_upload');

        $data = array(
            'name' => 'userfile',
            'type' => 'file',
            'size' => '20'
        );
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'name' => 'filetype',
            'type' => 'hidden',
            'id' => 'filetype',
            'value' => 'lembar_nilai_perusahaan2');
        echo form_input($data);
        echo "<br/>";
        $data = array(
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'value' => 'Upload'
        );
        echo form_submit($data);
        echo form_close();
        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <img src="" id="imagepreview" style="" >
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>