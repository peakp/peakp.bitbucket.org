README

5111100074 - Ebenhaezer Wiramarta Yogaswara
5111100112 - Peter Panyananda H.
5113100001 - Muhammad Adnan Yusuf

JOBDESC

Ebenhaezer - ewy
Peter P.H  - pph
M. Adnan Y.- mya

Pembagian Berdasarkan User
HALAMAN MAHASISWA
pph	Register
pph	1*Login
ewy	8*Page Akun Mahasiswa
-	8*Page Partner
ewy		Page Invite
pph	Page Form Pengajuan KP (Isi/edit form, save, print)
mya	Page Upload Surat Persetujuan KP
ewy	Page Form Bimbingan KP (Isi/edit form, save, print) - maksimal 8
mya	Page Upload Surat Pengumpulan Buku KP
mya	Page Upload Lembar Nilai Kenyataan
ewy	Page List Dokumen
ewy	Page Nilai Akhir KP

HALAMAN KOOR KP
-	1*Login 
ewy	2*Page Akun Dosen
pph	3*Page Persetujuan KP
ewy	4*Page isi Nilai Kenyataan
ewy	5*Page Dokumen Mhs
pph	6*Page Rekap Nilai Mhs

HALAMAN DOSBING
-	1*Login
-	2*Page Akun Dosen
pph	7*Page Nilai Ujian Lisan & Buku KP
	
HALAMAN ADMIN
pph	Login
pph	Pake Akun Admin
ewy	Page Data Master
-	3*Page Persetujuan KP
-	4*Page isi Nilai Kenyataan
-	5*Page Dokumen Mhs
-	6*Page Rekap Nilai Mhs
-	7*Page Nilai Ujian Lisan & Buku KP
NB:
1* sama dengan 1*

ONLINE
Akses website "peakp.bitbucket.org/KP"
Autentikasi Admin:
Username -> admin
Password -> admin

LOKAL
Program berikut dibutuhkan untuk menjalankan website KP secara lokal:
XAMPP - Apache & MySql

1. Download folder "KP" dari bitbucket, lalu taruh pada folder "htdocs" di dalam folder instalasi "XAMPP". eg : C:\xampp\htdocs

2. Jalankan XAMPP Control Panel, lalu aktifkan Apache dan MySql (pastikan port-port yang dibutuhkan untuk menjalankan Apache & MySql tidak terpakai, contoh program port nya bertabrakan dengan Apache : IIS)

3. Buka browser lalu akses "localhost/phpmyadmin"

4. Buat database baru dengan nama "kp", lalu import file bernama "kp.sql" dalam folder "KP" yang sudah Anda download dari bitbucket

5. Akses web dengan memasukkan alamat "localhost/KP"